import * as path from 'path';
import * as fse from 'fs-extra';
import * as prettier from 'prettier';
import template from '@babel/template';
import generate from '@babel/generator';
import * as t from '@babel/types';
import parseOpt, { OperationalTemplate } from './opt-parser';
import { ObjectNode, CCodePhrase } from './opt-parser';
import { Interval } from './openehr-rm';

const prettierConfigPath = path.resolve(
  path.join(__dirname, '../prettier.config.js')
);

function convertIntervalToStr(interval: Interval<number>) {
  let text = ' [';
  text += interval.lowerUnbounded ? '*' : interval.lower;
  text += '..';
  text += interval.upperUnbounded ? '*' : interval.upper;
  text += '] ';
  return text;
}

const findArchetypeId = (node: ObjectNode) => {
  if (typeof node.archetypeId === 'string') {
    return node.archetypeId;
  }
  if (node.parent?.parent) {
    return findArchetypeId(node.parent?.parent);
  }
  return null;
};

const createCallExpression = (node: ObjectNode, opt: OperationalTemplate) => {
  let code: string;
  let generatorName = node.rmTypeName;

  // TODO Fix DV_INTERVAL&lt;
  if (/^DV_INTERVAL&/.test(node.rmTypeName)) {
    generatorName = generatorName.slice(0, generatorName.indexOf('&'));
  }

  if (node.nodeId) {
    const name = opt.getTermDefinition(node.nodeId, findArchetypeId(node));
    // code = `${generatorName}/* ${name.text} ${convertIntervalToStr(
    //   node.occurrences
    // )} -- ${node.templatePath} */('${node.nodeId}', {})`;
    code = `${generatorName}/* ${name.text} ${convertIntervalToStr(
      node.occurrences
    )} -- */('${node.nodeId}', {})`;
  } else {
    code = `${generatorName}({})`;
  }

  const ast = template.expression.ast(code, { preserveComments: true });
  const propertiesArg = node.nodeId ? ast.arguments[1] : ast.arguments[0];
  const properties = propertiesArg.properties;

  if (typeof node.archetypeId === 'string') {
    properties.push(
      t.objectProperty(
        t.identifier('archetypeId'),
        t.stringLiteral(node.archetypeId)
      )
    );
  } else if (node.archetypeId instanceof RegExp) {
    const property = t.objectProperty(
      t.identifier('archetypeId'),
      t.stringLiteral('?')
    );
    t.addComment(property, 'trailing', ` ${node.archetypeId}`, true);
    properties.push(property);
  }

  if (node.templateId) {
    properties.push(
      t.objectProperty(
        t.identifier('templateId'),
        t.stringLiteral(node.templateId)
      )
    );
  }

  if (node.rmTypeName === 'HISTORY') {
    const eventsAttr = node.attributes.find(
      (attr) => attr.rmAttributeName === 'events'
    );
    if (eventsAttr) {
      properties.push(
        t.objectProperty(
          t.identifier('data'),
          createCallExpression(
            eventsAttr.children[0].attributes[0].children[0],
            opt
          )
        )
      );
    }
    return ast;
  }

  if (/^DV_/.test(node.rmTypeName)) {
    if (node.rmTypeName === 'DV_CODED_TEXT' && node.attributes.length > 0) {
      const definingCode = node.attributes.find(
        (attribute) => attribute.rmAttributeName === 'defining_code'
      );

      const cCodePhrase = definingCode?.children.find<CCodePhrase>(
        (child): child is CCodePhrase => child instanceof CCodePhrase
      );

      if (!cCodePhrase) {
        return ast;
      }

      const code = `CODE_PHRASE({
        codeString: '${cCodePhrase.codeList.join('|')}',
        terminologyId: '${cCodePhrase.terminologyId}'
      })`;
      const ast2 = template.expression.ast(code, { preserveComments: true });
      properties.push(t.objectProperty(t.identifier('definingCode'), ast2));
    } else {
      properties.push(
        t.objectProperty(t.identifier('value'), t.stringLiteral('?'))
      );
    }

    return ast;
  }

  node.attributes.forEach((attribute) => {
    let value;

    if (
      node.rmTypeName === 'ITEM_TREE' &&
      attribute.rmAttributeName === 'items'
    ) {
      value = t.arrayExpression(
        attribute.children.map((child) => createCallExpression(child, opt))
      );
    } else if (
      node.rmTypeName === 'COMPOSITION' &&
      attribute.rmAttributeName === 'content'
    ) {
      value = t.arrayExpression(
        attribute.children.map((child) => createCallExpression(child, opt))
      );
    } else if (
      node.rmTypeName === 'CLUSTER' &&
      attribute.rmAttributeName === 'items'
    ) {
      value = t.arrayExpression(
        attribute.children.map((child) => createCallExpression(child, opt))
      );
    } else {
      value = createCallExpression(attribute.children[0], opt);
    }

    const key = t.identifier(attribute.rmAttributeName);
    t.addComment(key, 'trailing', convertIntervalToStr(attribute.existence));

    properties.push(t.objectProperty(key, value));
  });

  return ast;
};

export default async function generateTemplate(optPath, outputPath) {
  const opt = await parseOpt(optPath);
  const ast = template.program.ast`
    function generate(data) {
      return ${createCallExpression(opt.definition, opt)}
    }
    module.exports = generate;
  `;
  const code = generate(ast, { retainLines: true }).code;

  const prettierConfig = await prettier.resolveConfig(prettierConfigPath);
  const outputFilePath = path.resolve(outputPath);
  const formattedCode = prettier.format(code, {
    ...prettierConfig,
    filepath: outputFilePath
  });
  fse.writeFileSync(outputFilePath, formattedCode, { encoding: 'utf-8' });
}
