import DataValueGenerator from './data-value';
import { DvDateTime } from '../openehr-rm';

export default class DvDateTimeGenerator extends DataValueGenerator {
  public value?;

  createRMInstance(row) {
    return new DvDateTime(this.value);
  }
}
