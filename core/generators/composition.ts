import LocatableGenerator from './locatable';
import CodePhraseGenerator from './code-phrase';
import DvCodedTextGenerator from './dv-coded-text';
import ContentItemGenerator from './content-item';
import { DvText, Composition } from '../openehr-rm';

export default class CompositionGenerator extends LocatableGenerator {
  public language?: CodePhraseGenerator;
  public territory?: CodePhraseGenerator;
  public category?: DvCodedTextGenerator;
  public content?: ContentItemGenerator[];

  createRMInstance(row) {
    const term = this.template.getTermDefinition(
      this.nodeId,
      this.findArchetypeId()
    );
    const name = new DvText(term.text);

    if (!this.language) {
      throw new Error('Language missing.');
    }

    if (!this.territory) {
      throw new Error('Territory missing.');
    }

    if (!this.category) {
      throw new Error('Category missing.');
    }

    if (!this.content) {
      throw new Error('Content missing.');
    }

    const language = this.language.createRMInstance();
    const territory = this.territory.createRMInstance();
    const category = this.category.createRMInstance();
    const content = this.content.map((c) => c.createRMInstance(row));
    const composition = new Composition(
      name,
      this.getNodeId(),
      language,
      territory,
      category,
      content
    );
    return composition;
  }
}
