import DataValueGenerator from './data-value';
import { DvText } from '../openehr-rm';

export default class DvTextGenerator extends DataValueGenerator {
  public value?;

  createRMInstance() {
    return new DvText(this.value);
  }
}
