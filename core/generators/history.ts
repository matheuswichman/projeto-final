import LocatableGenerator from './locatable';
import { ObjectNode } from '../opt-parser';
import { Event, DvText, History } from '../openehr-rm';

export default class HistoryGenerator extends LocatableGenerator {
  public data;

  createRMInstance(row) {
    const archetypeId = this.findArchetypeId();
    const archetypeNode = this.template.getArchetype(archetypeId);

    const searchNode = (node: ObjectNode): ObjectNode | null => {
      if (node.nodeId === this.nodeId) {
        return node;
      }
      for (const attribute of node.attributes) {
        for (const child of attribute.children) {
          const node = searchNode(child);
          if (node) {
            return node;
          }
        }
      }
      return null;
    };

    const historyNode = searchNode(archetypeNode!)!;
    const eventsAttribute = historyNode.attributes.find(
      (attr) => attr.rmAttributeName === 'events'
    );
    const eventNodeId = eventsAttribute?.children[0].nodeId;

    const historyTerm = this.template.getTermDefinition(
      this.nodeId,
      archetypeId
    );
    const eventTerm = this.template.getTermDefinition(eventNodeId, archetypeId);

    const events = [row].map((event) => {
      return new Event(
        new DvText(eventTerm.text),
        eventNodeId,
        this.data.generate(event)
      );
    });

    return new History(new DvText(historyTerm.text), this.nodeId, events);
  }
}
