import LocatableGenerator from './locatable';
import { DvText, Element } from '../openehr-rm';

export default class ElementGenerator extends LocatableGenerator {
  public value;

  createRMInstance(row) {
    const term = this.template.getTermDefinition(
      this.nodeId,
      this.findArchetypeId()
    );
    const name = new DvText(term.text);
    return new Element(name, this.nodeId, this.value.createRMInstance(row));
  }
}