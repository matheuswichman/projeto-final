import DataValueGenerator from './data-value';
import { DvCodedText, CodePhrase } from '../openehr-rm';
import { CodePhraseGenerator } from '.';

export default class DvCodedTextGenerator extends DataValueGenerator {
  public definingCode?: CodePhraseGenerator;

  createRMInstance() {
    const definingCodeFinal = this.definingCode.createRMInstance();
    const terminology = this.terminologyService.getTerminology(
      definingCodeFinal.terminologyId
    );
    return new DvCodedText(
      terminology.rubric(definingCodeFinal.codeString),
      definingCodeFinal
    );
  }
}
