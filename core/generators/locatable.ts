import { OperationalTemplate } from '../opt-parser';
import { Archetyped } from '../openehr-rm';
import TerminologyService from '../terminology-service';

export default abstract class LocatableGenerator {
  public nodeId: string | null;
  public parent: LocatableGenerator | null;
  public template: OperationalTemplate;
  public archetypeId?: string;
  public templateId?: string;
  public terminologyService?: TerminologyService;

  constructor(
    parent: LocatableGenerator | null,
    template: OperationalTemplate,
    terminologyService: TerminologyService,
    nodeId: string | null
  ) {
    this.nodeId = nodeId;
    this.parent = parent;
    this.template = template;
    this.terminologyService = terminologyService;
  }

  protected abstract createRMInstance(row);

  generate(row) {
    const instance = this.createRMInstance(row);
    if (this.archetypeId) {
      instance.archetypeDetails = new Archetyped(
        this.archetypeId,
        this.templateId
      );
    }
    return instance;
  }

  getNodeId() {
    return this.archetypeId || this.nodeId;
  }

  findArchetypeId(generator: LocatableGenerator = this) {
    if (generator.archetypeId) {
      return generator.archetypeId;
    }
    if (generator.parent) {
      return this.findArchetypeId(generator.parent);
    }
    debugger;
    throw new Error('Archetype ID not found.');
  }
}
