import LocatableGenerator from './locatable';

export default abstract class ContentItemGenerator extends LocatableGenerator {
  abstract createRMInstance(row);
}