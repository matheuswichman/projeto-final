import { CodePhrase } from '../openehr-rm';

export default class CodePhraseGenerator {
  public terminologyId?: string;
  public codeString?: string;

  createRMInstance() {
    return new CodePhrase(this.terminologyId, this.codeString);
  }
}