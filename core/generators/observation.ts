import ContentItemGenerator from './content-item';
import HistoryGenerator from './history';
import { DvText, Observation } from '../openehr-rm';

export default class ObservationGenerator extends ContentItemGenerator {
  public data?: HistoryGenerator;

  createRMInstance(row) {
    const term = this.template.getTermDefinition(
      this.nodeId,
      this.findArchetypeId()
    );
    const name = new DvText(term.text);

    if (!this.data) {
      throw new Error('History generator is missing.');
    }

    const data = this.data.createRMInstance(row);
    const observation = new Observation(name, this.getNodeId(), data);
    return observation;
  }
}
