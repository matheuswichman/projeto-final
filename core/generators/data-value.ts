import LocatableGenerator from './locatable';
import { OperationalTemplate } from '../opt-parser';
import TerminologyService from '../terminology-service';

export default abstract class DataValueGenerator {
  public parent: LocatableGenerator | null;
  public template: OperationalTemplate;
  public archetypeId?: string;
  public terminologyService?: TerminologyService;

  constructor(
    parent: LocatableGenerator | null,
    template: OperationalTemplate,
    terminologyService: TerminologyService
  ) {
    this.parent = parent;
    this.template = template;
    this.terminologyService = terminologyService;
  }

  abstract createRMInstance(row);
}
