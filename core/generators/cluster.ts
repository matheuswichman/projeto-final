import LocatableGenerator from './locatable';
import { DvText, Cluster } from '../openehr-rm';

export default class ClusterGenerator extends LocatableGenerator {
  public items;

  createRMInstance(row) {
    const term = this.template.getTermDefinition(
      this.nodeId,
      this.findArchetypeId()
    );
    const name = new DvText(term.text);
    return new Cluster(
      name,
      this.nodeId,
      (this.items || []).map((item) => item.createRMInstance(row))
    );
  }
}
