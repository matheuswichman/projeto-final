import LocatableGenerator from './locatable';
import { DvText, ItemTree } from '../openehr-rm';

export default class ItemTreeGenerator extends LocatableGenerator {
  public items?: LocatableGenerator[];

  createRMInstance(row) {
    const term = this.template.getTermDefinition(
      this.nodeId,
      this.findArchetypeId()
    );
    const name = new DvText(term.text);

    if (!this.items) {
      throw new Error('Item generator is missing.');
    }

    const items = this.items.map((generator) => generator.generate(row));
    return new ItemTree(name, this.nodeId, items);
  }
}