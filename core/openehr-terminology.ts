import * as fse from 'fs-extra';
import * as parser from 'fast-xml-parser';

export default class OpenEhrTerminology {
  private codeRubrics: Record<string, string> = {};

  public load(path) {
    const file = fse.readFileSync(path, 'utf-8');
    const parsedXml = parser.parse(file, { ignoreAttributes: false });

    parsedXml.terminology.group.forEach(({ concept }) => {
      const concepts = Array.isArray(concept) ? concept : [concept];
      concepts.forEach((concept) => {
        this.codeRubrics[concept['@_id']] = concept['@_rubric'];
      });
    });
  }

  public rubric(code) {
    return this.codeRubrics[code];
  }
}
