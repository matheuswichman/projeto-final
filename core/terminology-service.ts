export default class TerminologyService {
  private terminologies: Record<string, any> = {};

  public addTerminology(id, terminology) {
    this.terminologies[id] = terminology;
  }

  public getTerminology(id) {
    return this.terminologies[id];
  }
}
