#!/usr/bin/env node
import yargs from 'yargs';
import * as fs from 'fs';
import * as fse from 'fs-extra';
import * as path from 'path';
import parse from 'csv-parse';
import { hideBin } from 'yargs/helpers';
import generateTemplate from './template-generator';
import applyTemplate from './template-applier';
import { createGuid } from '../api/utils';
import * as child_process from 'child_process';

yargs(hideBin(process.argv))
  .command<{ optPath: string; outputPath: string }>(
    'generate <optPath> <outputPath>',
    'Generate template from OPT',
    () => {},
    async (argv) => {
      await generateTemplate(
        path.resolve(argv.optPath),
        path.resolve(argv.outputPath)
      );
      console.log('Template generated successfully.');
    }
  )
  .command(
    'validate <templatePath> <optPath>',
    'Validate template against OPT',
    () => {},
    (argv) => {
      console.log(argv);
    }
  )
  .command<{
    templatePath: string;
    optPath: string;
    csvPath: string;
    outputDir: string;
    format: string;
  }>(
    'apply <templatePath> <optPath> <csvPath> <outputDir>',
    'Validate template and apply data',
    (yargs) => {
      return yargs.option('format', {
        describe: 'The format to output',
        choices: ['xml', 'json'],
        default: 'json'
      });
    },
    async (argv) => {
      const conversions = [];
      const begin = Date.now();

      console.log(`PID: ${process.pid}`);

      const metricsCollector = child_process.spawn(
        path.resolve(path.join(__dirname, '../process-metrics-collector.sh')),
        [String(process.pid)]
      );

      fse.mkdirSync(argv.outputDir, {
        mode: 0o777,
        recursive: true
      });

      fs.createReadStream(argv.csvPath)
        .pipe(parse({ columns: true, delimiter: ';' }))
        .on('readable', function () {
          let row;
          while ((row = this.read())) {
            const promise = new Promise<void>(async (resolve, reject) => {
              try {
                const output = await applyTemplate(
                  argv.optPath,
                  argv.templatePath,
                  row,
                  argv.format
                );
                const id = createGuid();
                const fileName = `${id}.${argv.format}`;
                const outputPath = path.join(argv.outputDir, fileName);
                fse.writeFileSync(outputPath, output);
                console.log(`File '${fileName}' saved successfully.`);
                resolve();
              } catch (error) {
                reject(error);
              }
            });
            conversions.push(promise);
          }
        })
        .on('end', async () => {
          const results = await Promise.allSettled(conversions);

          const fails = Object.values(results).filter(
            (promise): promise is PromiseRejectedResult =>
              promise.status === 'rejected'
          );
          fails.forEach((build) => {
            console.error(build.reason);
          });
          if (fails.length > 0) {
            process.exit(1);
          }
          const seconds = (Date.now() - begin) / 1000;
          console.log(
            `${
              results.length
            } files were successfully converted in ${seconds.toFixed(
              2
            )} seconds.`
          );
          metricsCollector.kill();
        });
    }
  )
  .help().argv;
