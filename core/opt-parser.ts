import * as fse from 'fs-extra';
import * as parser from 'fast-xml-parser';
import { Interval } from './openehr-rm';

export class AttributeNode {
  public rmAttributeName: string;
  public type: string;
  public parent?: ObjectNode;
  public existence: Interval<number>;
  private _children?: ObjectNode[];

  constructor(
    rmAttributeName: string,
    type: string,
    existence: Interval<number>,
    children: ObjectNode[]
  ) {
    this.rmAttributeName = rmAttributeName;
    this.type = type;
    this.existence = existence;
    this.children = children;
  }

  public set children(children: ObjectNode[]) {
    children.forEach((child) => {
      child.parent = this;
    });
    this._children = children;
  }

  public get children() {
    return this._children || [];
  }
}

export class TermDefinition {
  public text: string;
  public description: string;

  constructor(text: string, description: string) {
    this.text = text;
    this.description = description;
  }
}

export class ObjectNode {
  public rmTypeName: string;
  public nodeId: string;
  public templateId?: string;
  public archetypeId?: string | RegExp;
  public templatePath: string;
  public termDefinitions?: Record<string, TermDefinition>;
  public parent?: AttributeNode;
  public occurrences: Interval<number>;
  private _attributes?: AttributeNode[];

  constructor(data) {
    this.rmTypeName = data.rmTypeName;
    this.nodeId = data.nodeId;
    this.templateId = data.templateId;
    this.archetypeId = data.archetypeId;
    this.attributes = data.attributes;
    this.templatePath = data.templatePath;
    this.termDefinitions = data.termDefinitions;
    this.occurrences = data.occurrences;
  }

  public set attributes(attributes: AttributeNode[]) {
    attributes.forEach((attribute) => {
      attribute.parent = this;
    });
    this._attributes = attributes;
  }

  public get attributes() {
    return this._attributes || [];
  }
}

export class CCodePhrase extends ObjectNode {
  public codeList: string[];
  public terminologyId: string;

  constructor(data) {
    super(data);
    this.codeList = data.codeList;
    this.terminologyId = data.terminologyId;
  }
}

export class OperationalTemplate {
  public uid: string;
  public templateId: string;
  public concept: string;
  public language: string;
  public definition: ObjectNode;

  constructor(data) {
    this.uid = data.uid;
    this.templateId = data.templateId;
    this.concept = data.concept;
    this.language = data.language;
    this.definition = data.definition;
  }

  getArchetype(archetypeId) {
    function searchNode(node: ObjectNode): ObjectNode | null {
      if (node.archetypeId === archetypeId) {
        return node;
      }

      for (const attribute of node.attributes) {
        for (const child of attribute.children) {
          const node = searchNode(child);
          if (node) {
            return node;
          }
        }
      }

      return null;
    }

    return searchNode(this.definition);
  }

  getTermDefinition(nodeId, archetypeId) {
    const node = this.getArchetype(archetypeId);

    if (!node) {
      return { text: 'ARCHETYPE NOT DEFINED' };
      // throw new Error(`Archetype '${archetypeId}' not found.`);
    }

    if (!node.termDefinitions) {
      throw new Error('Archetype has no term definitions.');
    }

    return node.termDefinitions[nodeId];
  }
}

function castToArray(value) {
  return (Array.isArray(value) ? (value as any[]) : [value]).filter(Boolean);
}

function parseCodePhrase(node) {
  return `${node.terminology_id.value}::${node.code_string}`;
}

function parseInterval(node) {
  return {
    lower: node.lower,
    upper: node.upper,
    lowerUnbounded: node.lower_unbounded,
    upperUnbounded: node.upper_unbounded,
    lowerIncluded: node.lower_included,
    upperIncluded: node.upper_included
  };
}

function parseAttribute(node: any, path: string) {
  let templatePath = path;
  if (path !== '/') {
    templatePath += `/${node.rm_attribute_name}`;
  } else {
    templatePath += node.rm_attribute_name;
  }

  const result = new AttributeNode(
    node.rm_attribute_name,
    node['@_xsi:type'],
    parseInterval(node.existence),
    castToArray(node.children).map((child) => parseObject(child, templatePath))
  );

  return result;
}

function parseObject(node: any, path: string) {
  let templatePath = path;
  if (path !== '/') {
    if (node.archetype_id?.value) {
      templatePath += `[${node.archetype_id.value}]`;
    } else if (node.node_id) {
      templatePath += `[${node.node_id}]`;
    }
  }

  let objectToReturn: ObjectNode;

  const occurrences = parseInterval(node.occurrences);

  const type = node['@_xsi:type'];

  if (type === 'C_CODE_PHRASE') {
    objectToReturn = new CCodePhrase({
      templatePath,
      nodeId: node.node_id,
      attributes: [],
      rmTypeName: node.rm_type_name,
      terminologyId: node.terminology_id.value,
      codeList: castToArray(node.code_list),
      occurrences
    });
  } else {
    let archetypeId;

    if (type === 'ARCHETYPE_SLOT') {
      const { pattern } = node.includes.expression.right_operand.item;
      archetypeId = new RegExp(pattern);
    } else if (node.archetype_id) {
      archetypeId = node.archetype_id.value;
    }

    objectToReturn = new ObjectNode({
      templatePath,
      attributes: [],
      nodeId: node.node_id,
      rmTypeName: node.rm_type_name,
      archetypeId,
      templateId: node.template_id?.value,
      occurrences
    });
  }

  const attributes = castToArray(node.attributes);
  objectToReturn.attributes = attributes.map((attribute) =>
    parseAttribute(attribute, templatePath)
  );

  if (node.term_definitions) {
    objectToReturn.termDefinitions = node.term_definitions.reduce(
      (acc, term) => {
        const items = castToArray(term.items);
        const text = items.find((i) => i['@_id'] === 'text');
        const description = items.find((i) => i['@_id'] === 'description');
        const code = term['@_code'];

        acc[code] = new TermDefinition(
          text['#text'],
          description ? description['#text'] : null
        );

        return acc;
      },
      {}
    );
  }

  return objectToReturn;
}

export default async function parseOpt(optPath: string) {
  const optData = await fse.readFile(optPath, 'utf-8');
  const { template } = parser.parse(optData, { ignoreAttributes: false });

  return new OperationalTemplate({
    uid: template.uid.value,
    concept: template.concept,
    templateId: template.template_id.value,
    language: parseCodePhrase(template.language),
    definition: parseObject(template.definition, '/')
  });
}
