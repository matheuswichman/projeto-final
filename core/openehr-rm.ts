function rmType(type: string) {
  return function <T extends { new (...args: any[]): {} }>(constructor: T) {
    return class extends constructor {
      _rmType = type;
    };
  };
}

abstract class Locatable {
  public archetypeNodeId: string;
  public name: DvText;
  public archetypeDetails?: Archetyped;

  constructor(name, nodeId) {
    this.name = name;
    this.archetypeNodeId = nodeId;
  }
}

export interface Interval<T> {
  lower: T;
  upper: T;
  lowerIncluded: boolean;
  upperIncluded: boolean;
  lowerUnbounded: boolean;
  upperUnbounded: boolean;
}

@rmType('ARCHETYPED')
export class Archetyped {
  public archetypeId: string;
  public templateId?: string;
  public rmVersion: string = '1.1.0';

  constructor(archetypeId, templateId) {
    this.archetypeId = archetypeId;
    this.templateId = templateId;
  }
}

@rmType('EVENT')
export class Event extends Locatable {
  public data: any;

  constructor(name, nodeId, data) {
    super(name, nodeId);
    this.data = data;
  }
}

@rmType('HISTORY')
export class History extends Locatable {
  public events: Event[];

  constructor(name, nodeId, events) {
    super(name, nodeId);
    this.events = events;
  }
}

@rmType('OBSERVATION')
export class Observation extends Locatable {
  public data?: History;

  constructor(name, nodeId, data) {
    super(name, nodeId);
    this.data = data;
  }
}

@rmType('COMPOSITION')
export class Composition extends Locatable {
  public language: CodePhrase;
  public territory: CodePhrase;
  public category: DvCodedText;
  // TODO public context: EventContext;
  // TODO public composer: PartyProxy;
  public content: any[];

  constructor(name, nodeId, language, territory, category, content) {
    super(name, nodeId);
    this.language = language;
    this.territory = territory;
    this.category = category;
    this.content = content;
  }
}

export class CodePhrase {
  public terminologyId: string;
  public codeString: string;

  constructor(terminologyId, codeString) {
    this.terminologyId = terminologyId;
    this.codeString = codeString;
  }
}

interface DataValue {}

@rmType('DV_TEXT')
export class DvText implements DataValue {
  public value: string;

  constructor(value) {
    this.value = value;
  }
}

@rmType('DV_CODED_TEXT')
export class DvCodedText extends DvText {
  public definingCode: CodePhrase;

  constructor(value, definingCode) {
    super(value);
    this.definingCode = definingCode;
  }
}

@rmType('DV_DATE_TIME')
export class DvDateTime extends DvText {
  constructor(value) {
    super(value);
  }
}

abstract class Item extends Locatable {
  constructor(name, nodeId) {
    super(name, nodeId);
  }
}

@rmType('ELEMENT')
export class Element extends Item {
  public value: DataValue;

  constructor(name, nodeId, value) {
    super(name, nodeId);
    this.value = value;
  }
}

@rmType('CLUSTER')
export class Cluster extends Item {
  public items: Item[];

  constructor(name, nodeId, items) {
    super(name, nodeId);
    this.items = items;
  }
}

@rmType('ITEM_TREE')
export class ItemTree extends Locatable {
  public items: Item[];

  constructor(name, nodeId, items) {
    super(name, nodeId);
    this.items = items;
  }
}
