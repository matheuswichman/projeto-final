import * as fse from 'fs-extra';
import * as vm from 'vm';
import * as path from 'path';
import upperCase from 'lodash/upperCase';
import snakeCase from 'lodash/snakeCase';
import get from 'lodash/get';
import { j2xParser } from 'fast-xml-parser';
import parseOpt from './opt-parser';
import * as generators from './generators';
import OpenEhrTerminology from './openehr-terminology';
import TerminologyService from './terminology-service';

function noopGenerator() {
  return {
    createRMInstance: () => null
  };
}

function convertToXml(instance) {
  const transverse = (node) => {
    if (node === undefined || node === null) {
      return '?';
    }

    return Object.entries(node).reduce((acc, [key, value]) => {
      if (key === 'parent') {
        return acc;
      }
      const convertedKey = snakeCase(key);
      if (key === '_rmType') {
        acc['@_xsi:type'] = value;
      } else if (key === 'archetypeNodeId') {
        acc['@_archetype_node_id'] = value;
      } else if (Array.isArray(value)) {
        acc[convertedKey] = value.map(transverse);
      } else if (typeof value === 'object') {
        acc[convertedKey] = transverse(value);
      } else {
        acc[convertedKey] = value;
      }
      return acc;
    }, {});
  };

  const jsonToXml = new j2xParser({
    format: true,
    attributeNamePrefix: '@_',
    ignoreAttributes: false
  });

  const header = '<?xml version="1.0"?>';
  const body = jsonToXml.parse({ entry: transverse(instance) });
  return `${header}\n${body}`;
}

function convertToJson(instance) {
  return JSON.stringify(
    instance,
    (key, value) => (key === 'parent' ? undefined : value),
    2
  );
}

const customTerminology = {
  rubric: (code) => {
    if (code === 'at0024') {
      return 'Absent';
    } else if (code === 'at0023') {
      return 'Present';
    } else if (code === 'at0027') {
      return 'Unknown';
    }
    return code
  }
};

export default async function applyTemplate(
  optPath,
  templatePath,
  input,
  format
) {
  const opt = await parseOpt(optPath);
  const template = fse.readFileSync(templatePath, { encoding: 'utf-8' });

  const openEhrTerminology = new OpenEhrTerminology();
  openEhrTerminology.load(
    path.resolve(__dirname, '../res/openehr_terminology.xml')
  );
  const terminologyService = new TerminologyService();
  terminologyService.addTerminology('openehr', openEhrTerminology);
  terminologyService.addTerminology('local', customTerminology);
  terminologyService.addTerminology('custom', customTerminology);

  const context = {
    module: { exports: null } as any,
    $: (path) => get(input, path),
    EVENT_CONTEXT: noopGenerator,
    DV_IDENTIFIER: noopGenerator,
    ADMIN_ENTRY: noopGenerator,
    EVALUATION: noopGenerator,
    DV_DURATION: noopGenerator,
    DV_BOOLEAN: noopGenerator,
    DV_QUANTITY: noopGenerator,
    DV_COUNT: noopGenerator,
    DV_URI: noopGenerator,
    ISM_TRANSITION: noopGenerator,
    ACTION: noopGenerator,
    SECTION: noopGenerator
  };

  Object.entries(generators).forEach(([name, fn]) => {
    const rmType = upperCase(name.replace('Generator', '')).replace(/\s/g, '_');

    context[rmType] = (...args) => {
      const nodeId = args.length === 2 ? args[0] : null;
      const attributes = args.length === 2 ? args[1] : args[0];
      const instance = new fn(null, opt, terminologyService, nodeId);

      Object.entries<any>(attributes).forEach(([key, value]) => {
        try {
          instance[key] = value;
          if (Array.isArray(instance[key])) {
            instance[key].forEach((item) => {
              item.parent = instance;
            });
          } else {
            instance[key].parent = instance;
          }
        } catch (error) {}
      });

      return instance;
    };
  });

  vm.createContext(context);
  vm.runInContext(template, context);

  const generator = context.module.exports(input);
  const instance = generator.createRMInstance(input);

  return format === 'xml' ? convertToXml(instance) : convertToJson(instance);
}
