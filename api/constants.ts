import * as path from 'path';

export const DATA_DIR = path.resolve(__dirname, '../data');

export const DATASETS_DIR = path.join(DATA_DIR, 'datasets');

export const TEMPLATES_DIR = path.join(DATA_DIR, 'templates');

export const PROJECTS_DIR = path.join(DATA_DIR, 'projects');

export const JOBS_DIR = path.join(DATA_DIR, 'jobs');
