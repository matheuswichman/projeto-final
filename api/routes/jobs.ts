import express from 'express';
import amqp from 'amqplib';
import * as path from 'path';
import * as fse from 'fs-extra';
import { createGuid } from '../utils';
import { JOBS_DIR, PROJECTS_DIR } from '../constants';

const router = express.Router();

let connection: amqp.Connection;
amqp.connect('amqp://localhost').then((conn) => {
  connection = conn;
});

const queue = 'jobs';

router.post('/create/:projectId', async (req, res) => {
  const channel = await connection.createChannel();
  const { projectId } = req.params;
  const id = createGuid();

  await channel.assertQueue(queue, { durable: true });
  const options = { messageId: id };
  const manifestPath = path.resolve(PROJECTS_DIR, `${projectId}.json`);
  const project = fse.readFileSync(manifestPath, { encoding: 'utf-8' });
  const json = JSON.parse(project);
  const content = Buffer.from(
    JSON.stringify({ id, projectId, datasetId: json.datasetId })
  );
  const result = channel.sendToQueue(queue, content, options);

  if (!result) {
    return res.status(400).send("Can't create job.");
  }

  const jobPath = path.resolve(JOBS_DIR, `${id}.json`);
  fse.writeJSONSync(jobPath, { id, status: 'PENDING' });

  res.send({ id });
});

router.get('/', function (req, res) {
  const files = fse.readdirSync(JOBS_DIR);
  const datasets = [];

  files.forEach((file) => {
    if (path.extname(file) !== '.json') {
      return;
    }

    const jobPath = path.join(JOBS_DIR, file);
    const manifest = fse.readFileSync(jobPath, { encoding: 'utf-8' });
    const json = JSON.parse(manifest);
    datasets.push({
      id: path.basename(jobPath, '.json'),
      status: json.status,
      totalRows: json.totalRows || 0,
      convertedRows: json.convertedRows || 0
    });
  });

  res.json(datasets);
});

router.get('/:id', (req, res) => {
  const { id } = req.params;
  const jobPath = path.resolve(JOBS_DIR, `${id}.json`);
  console.log(jobPath);
  const json = fse.readJSONSync(jobPath);
  res.send(json);
});

export default router;
