import express from 'express';
import * as path from 'path';
import * as fse from 'fs-extra';
import { PROJECTS_DIR, TEMPLATES_DIR } from '../constants';
import generateTemplate from '../../core/template-generator';
import { createGuid } from '../utils';
import applyTemplate from '../../core/template-applier';

const router = express.Router();

router.get('/', function (req, res) {
  const files = fse.readdirSync(PROJECTS_DIR);
  const datasets = [];

  files.forEach((file) => {
    if (path.extname(file) !== '.json') {
      return;
    }

    const projectPath = path.join(PROJECTS_DIR, file);
    const manifest = fse.readFileSync(projectPath, { encoding: 'utf-8' });
    const json = JSON.parse(manifest);
    datasets.push({ id: path.basename(projectPath, '.json'), name: json.name });
  });

  res.json(datasets);
});

router.post('/create', async function (req, res) {
  const { name, templateId } = req.body;
  const id = createGuid();

  const optPath = path.resolve(TEMPLATES_DIR, `${templateId}.opt`);
  await generateTemplate(optPath, path.resolve(PROJECTS_DIR, `${id}.js`));

  const manifest = { name, templateId };
  fse.writeJSONSync(path.resolve(PROJECTS_DIR, `${id}.json`), manifest);

  res.status(201).json({ id: id });
});

router.put('/:id', function (req, res) {
  const { id } = req.params;
  const code = req.body;
  const projectPath = path.resolve(PROJECTS_DIR, `${id}.js`);
  fse.writeFileSync(projectPath, code);
  res.send({ lastUpdate: new Date().toISOString() });
});

router.put('/:id/regenerate', async function (req, res) {
  const { id } = req.params;
  const { templateId } = req.body;

  const optPath = path.resolve(TEMPLATES_DIR, `${templateId}.opt`);
  await generateTemplate(optPath, path.resolve(PROJECTS_DIR, `${id}.js`));

  res.send({ lastUpdate: new Date().toISOString() });
});

router.put('/:id/info', function (req, res) {
  const { id } = req.params;
  const { name, templateId, datasetId } = req.body;
  const projectPath = path.resolve(PROJECTS_DIR, `${id}.json`);
  const project = fse.readFileSync(projectPath, { encoding: 'utf-8' });
  const json = JSON.parse(project);
  const updatedProject = { ...json, name, templateId, datasetId };
  fse.writeJSONSync(projectPath, updatedProject);
  res.json(updatedProject);
});

router.get('/:id', function (req, res) {
  const { id } = req.params;
  const projectPath = path.resolve(PROJECTS_DIR, `${id}.js`);
  const project = fse.readFileSync(projectPath, { encoding: 'utf-8' });
  res.send(project);
});

router.get('/:id/info', function (req, res) {
  const { id } = req.params;
  const manifestPath = path.resolve(PROJECTS_DIR, `${id}.json`);
  const projectPath = path.resolve(PROJECTS_DIR, `${id}.js`);
  const project = fse.readFileSync(manifestPath, { encoding: 'utf-8' });
  const json = JSON.parse(project);

  const fd = fse.openSync(projectPath, 'r');
  const stats = fse.fstatSync(fd);
  const lastUpdate = new Date(stats.mtimeMs);

  res.json({ ...json, lastUpdate: lastUpdate.toISOString() });
});

router.get('/:id/preview', async function (req, res) {
  const { id } = req.params;
  const data = Buffer.from(
    req.query.data as string,
    'base64'
  ) as unknown as string;
  const rowData = JSON.parse(data);

  const manifestPath = path.resolve(PROJECTS_DIR, `${id}.json`);
  const projectPath = path.resolve(PROJECTS_DIR, `${id}.js`);
  const project = fse.readFileSync(manifestPath, { encoding: 'utf-8' });
  const projectJson = JSON.parse(project);
  const optPath = path.resolve(TEMPLATES_DIR, `${projectJson.templateId}.opt`);

  try {
    const output = await applyTemplate(optPath, projectPath, rowData, 'json');
    res.type('application/json').send(output);
  } catch (error) {
    console.log(error);
    res.send(error.message);
  }
});

export default router;
