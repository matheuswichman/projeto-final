import express from 'express';
import * as path from 'path';
import * as fse from 'fs-extra';
import { TEMPLATES_DIR } from '../constants';
import parseOpt from '../../core/opt-parser';
import { createGuid } from '../utils';
import { UploadedFile } from 'express-fileupload';

const router = express.Router();

router.get('/', async function (req, res) {
  const files = fse.readdirSync(TEMPLATES_DIR);
  const promises = [];

  files.forEach((file) => {
    if (path.extname(file) !== '.opt') {
      return;
    }
    const templatePath = path.join(TEMPLATES_DIR, file);
    promises.push([file, parseOpt(templatePath)]);
  });

  const parsedOpts = await Promise.all(promises);
  const templates = [];
  parsedOpts.forEach(([file, parsedOpt]) => {
    const id = path.basename(file, '.opt');
    templates.push({ id, name: parsedOpt.concept, fileName: file });
  });

  res.json(templates);
});

router.post('/upload', function (req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No file was uploaded.');
  }

  const file = req.files.template as UploadedFile;
  if (!file.name.endsWith('.opt')) {
    return res.status(400).send('File must have .opt extension.');
  }

  const id = createGuid();
  const fileName = path.basename(file.name, '.opt');
  const templatePath = path.resolve(
    TEMPLATES_DIR,
    `${fileName} ${id.slice(0, 5)}.opt`
  );

  file.mv(templatePath, (err) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.send();
  });
});

export default router;
