import express from 'express';
import * as path from 'path';
import * as fse from 'fs-extra';
import parse from 'csv-parse/lib/sync';
import { UploadedFile } from 'express-fileupload';
import { DATASETS_DIR } from '../constants';
import { createGuid } from '../utils';

const router = express.Router();

router.get('/', function (req, res) {
  const files = fse.readdirSync(DATASETS_DIR);
  const datasets = [];

  files.forEach((file) => {
    if (path.extname(file) !== '.json' || file.endsWith('-preview.json')) {
      return;
    }

    const datasetPath = path.join(DATASETS_DIR, file);
    const manifest = fse.readFileSync(datasetPath, { encoding: 'utf-8' });
    const id = path.basename(file, '.json');
    const json = JSON.parse(manifest);
    datasets.push({ id, ...json });
  });

  res.json(datasets);
});

router.get('/:id', function (req, res) {
  const { id } = req.params;
  const previewPath = path.resolve(DATASETS_DIR, `${id}-preview.json`);
  const preview = fse.readFileSync(previewPath, { encoding: 'utf-8' });
  const json = JSON.parse(preview);
  res.send(json);
});

router.post('/upload', function (req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No file was uploaded.');
  }

  const file = req.files.dataset as UploadedFile;
  if (!file.name.endsWith('.csv')) {
    return res.status(400).send('File must have .csv extension.');
  }

  const id = createGuid();
  const csvPath = path.resolve(DATASETS_DIR, `${id}.csv`);
  const manifestPath = path.resolve(DATASETS_DIR, `${id}.json`);
  const previewPath = path.resolve(DATASETS_DIR, `${id}-preview.json`);

  file.mv(csvPath, (err) => {
    if (err) {
      return res.status(500).send(err);
    }

    const csv = fse.readFileSync(csvPath, { encoding: 'utf-8' });
    const [headers] = parse(csv, { delimiter: ';', to_line: 1 });
    const records = parse(csv, { columns: true, delimiter: ';' });
    const fileName = path.basename(file.name, '.csv');
    const manifest = {
      name: `${fileName} ${id.slice(0, 5)}`,
      columns: headers,
      rowCount: records.length
    };
    fse.writeJSONSync(manifestPath, manifest);
    fse.writeJSONSync(
      previewPath,
      records.slice(0, 100).map((record, index) => ({ id: index, ...record }))
    );
    res.send();
  });
});

export default router;
