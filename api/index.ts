import express from 'express';
import fileUpload from 'express-fileupload';
import fileManager from 'express-file-manager';
import path from 'path';

import datasetsRouter from './routes/datasets';
import templatesRouter from './routes/templates';
import projectsRouter from './routes/projects';
import jobsRouter from './routes/jobs';
import { JOBS_DIR } from './constants';

const app = express();

app.use(express.text({ limit: '50mb' }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(fileUpload());

app.use('/filemanager', fileManager(path.resolve(JOBS_DIR)));

const port = 3000;

app.use('/datasets', datasetsRouter);
app.use('/templates', templatesRouter);
app.use('/projects', projectsRouter);
app.use('/jobs', jobsRouter);

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`REST API listening at http://localhost:${port}`);
});
