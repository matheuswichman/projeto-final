#!/bin/bash

# process id to monitor
pid=$1

if [ -z $1 ]; then
  echo "ERROR: Process ID not specified."
  echo
  echo "Usage: $(basename "$0") <PID>"
  exit 1
fi

# check if process exists
kill -0 $pid > /dev/null 2>&1
pid_exist=$?

if [ $pid_exist != 0 ]; then
  echo "ERROR: Process ID $pid not found."
  exit 1
fi

current_time=$(date +"%Y_%m_%d_%H%M")
dir_name="metrics"
csv_filename="${dir_name}/${pid}-${current_time}.csv"

# create data directory
mkdir -p $dir_name

echo "Writing data to CSV file $csv_filename..."
touch $csv_filename

# write CSV headers
echo "Time,CPU,Memory,Disk" >> $csv_filename

# check if process exists
kill -0 $pid > /dev/null 2>&1
pid_exist=$?

# collect until process exits
while [ $pid_exist == 0 ]; do
  # check if process exists
  kill -0 $pid > /dev/null 2>&1
  pid_exist=$?

  if [ $pid_exist == 0 ]; then
    # read cpu and mem percentages
    timestamp=$(date +"%b %d %H:%M:%S")
    cpu_mem_usage=$(top -b -n 1 | grep -w -E "^ *$pid" | awk '{print $9 "," $10}')
    disk_usage=$(sudo iotop -b -n 1 | grep -w -E "^ *$pid" | awk '{print $8}')

    # write CSV row
    echo "$timestamp,$cpu_mem_usage,$disk_usage" >> $csv_filename
    sleep 5
  fi
done
