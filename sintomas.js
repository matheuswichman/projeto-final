function generate(data) {
  return OBSERVATION(
    /* Symptom/sign screening questionnaire  [1..1]  -- */ 'at0000',
    {
      archetypeId: 'openEHR-EHR-OBSERVATION.symptom_sign_screening.v0',
      templateId: 'Symptom/sign screening',
      data /* [1..1] */: HISTORY(/* History  [1..1]  -- */ 'at0001', {
        data: ITEM_TREE(/* Tree  [1..1]  -- */ 'at0003', {
          items /* [0..1] */: [
            ELEMENT(/* Screening purpose  [0..1]  -- */ 'at0034', {
              value /* [0..1] */: DV_CODED_TEXT({
                definingCode: CODE_PHRASE({
                  codeString: '840544004',
                  terminologyId: 'SNOMED-CT'
                })
              })
            }),
            CLUSTER(/* Specific symptom/sign  [0..1]  -- */ 'at0022', {
              items /* [1..1] */: [
                ELEMENT(/* Symptom or sign name  [1..1]  -- */ 'at0004', {
                  value /* [0..1] */: DV_CODED_TEXT({
                    definingCode: CODE_PHRASE({
                      codeString:
                        '315642008|49727002|386661006|267036007|162397003',
                      terminologyId: 'SNOMED-CT'
                    })
                  })
                }),
                ELEMENT(/* Presence?  [0..1]  -- */ 'at0005', {
                  value /* [0..1] */: DV_CODED_TEXT({
                    definingCode: CODE_PHRASE({
                      codeString: 'at0023|at0024|at0027',
                      terminologyId: 'local'
                    })
                  })
                }),
                ELEMENT(/* Onset  [0..1]  -- */ 'at0006', {
                  value /* [0..1] */: DV_DATE_TIME({ value: $('SYMPTOM_ONSET_DATE') })
                }),
                ELEMENT(/* Comment  [0..1]  -- */ 'at0035', {
                  value /* [0..1] */: DV_TEXT({ value: '?' })
                })
              ]
            }),
            CLUSTER(/* Specific symptom/sign  [0..*]  -- */ 'at0022', {
              items /* [1..1] */: [
                ELEMENT(/* Symptom or sign name  [1..1]  -- */ 'at0004', {
                  value /* [0..1] */: DV_CODED_TEXT({
                    definingCode: CODE_PHRASE({
                      codeString:
                        '315642008|49727002|386661006|267036007|162397003',
                      terminologyId: 'SNOMED-CT'
                    })
                  })
                }),
                ELEMENT(/* Presence?  [0..1]  -- */ 'at0005', {
                  value /* [0..1] */: DV_CODED_TEXT({
                    definingCode: CODE_PHRASE({
                      codeString: 'at0023|at0024|at0027',
                      terminologyId: 'local'
                    })
                  })
                }),
                ELEMENT(/* Onset  [0..1]  -- */ 'at0006', {
                  value /* [0..1] */: DV_DATE_TIME({ value: '?' })
                }),
                ELEMENT(/* Comment  [0..1]  -- */ 'at0035', {
                  value /* [0..1] */: DV_TEXT({ value: '?' })
                })
              ],
              name /* [1..1] */: DV_TEXT({ value: '?' })
            }),
            CLUSTER(/* Additional details  [0..*]  -- */ 'at0026', {
              archetypeId: '?' // /.*/
            }),
            ELEMENT(/* Comment  [0..1]  -- */ 'at0025', {
              value /* [0..1] */: DV_TEXT({ value: '?' })
            })
          ]
        })
      }),
      protocol /* [0..1] */: ITEM_TREE(/* Item tree  [1..1]  -- */ 'at0007', {
        items /* [0..1] */: [
          CLUSTER(/* Extension  [0..*]  -- */ 'at0021', {
            archetypeId: '?' // /.*/
          })
        ]
      }),
      name /* [1..1] */: DV_TEXT({ value: '?' })
    }
  );
}
module.exports = generate;
