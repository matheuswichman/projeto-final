import * as React from 'react';
import Skeleton from '@mui/material/Skeleton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import HomeBlock from './HomeBlock';
import { UploadDialog } from './UploadDialog';

const Datasets = () => {
  const [datasets, setDatasets] = React.useState<any[] | null>(null);
  const [open, setOpen] = React.useState(false);
  const [uploading, setUploading] = React.useState(false);

  const load = React.useCallback(async () => {
    const response = await fetch('/api/datasets');
    const datasets = await response.json();
    setDatasets(datasets);
  }, []);

  React.useEffect(() => {
    load();
  }, [load]);

  const handleAddClick = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleFileSelected = async (file) => {
    const data = new FormData();
    data.append('dataset', file);
    setUploading(true);

    try {
      const result = await fetch('/api/datasets/upload', {
        method: 'POST',
        body: data
      });
      if (!result.ok) {
        throw new Error('Something went wrong.');
      }
      setUploading(false);
      load();
    } catch (error: any) {
      setUploading(false);
      alert(error.message);
    } finally {
      setOpen(false);
    }
  };

  const renderContent = () => {
    if (!datasets) {
      return (
        <React.Fragment>
          <Skeleton animation="wave" />
          <Skeleton animation="wave" />
          <Skeleton animation="wave" />
        </React.Fragment>
      );
    }

    return (
      <List>
        {datasets.map((dataset) => (
          <ListItem key={dataset.id} disablePadding dense>
            <ListItemButton>
              <ListItemText
                primary={dataset.name}
                secondary={`Rows: ${dataset.rowCount}`}
              />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    );
  };

  return (
    <React.Fragment>
      <UploadDialog
        title="Upload dataset"
        open={open}
        loading={uploading}
        onClose={handleClose}
        onFileSelected={handleFileSelected}
      />
      <HomeBlock title="Datasets" onAddClick={handleAddClick}>
        {renderContent()}
      </HomeBlock>
    </React.Fragment>
  );
};

export default Datasets;
