import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Projects from './Projects';
import Datasets from './Datasets';
import Templates from './Templates';
import Jobs from './Jobs';

const Home = () => {
  const [domReady, setDomReady] = React.useState(false);
  const [projects, setProjects] = React.useState(null);

  React.useEffect(() => {
    setDomReady(true);
  });

  return (
    <React.Fragment>
      {domReady &&
        ReactDOM.createPortal(
          <Typography variant="h6">Home</Typography>,
          document.getElementById('title')!
        )}
      <Container maxWidth="xl" sx={{ mt: 4 }}>
        <Grid container spacing={2}>
          <Grid md={6} item>
            <Projects />
          </Grid>
          <Grid md={6} item>
            <Datasets />
          </Grid>
          <Grid md={6} item>
            <Templates />
          </Grid>
          <Grid md={6} item>
            <Jobs />
          </Grid>
        </Grid>
      </Container>
    </React.Fragment>
  );
};

export default Home;
