import * as React from 'react';
import * as monaco from 'monaco-editor';

// @ts-ignore
self.MonacoEnvironment = {
  getWorkerUrl: function (_moduleId: any, label: string) {
    if (label === 'json') {
      return '/json.worker.bundle.js';
    }
    if (label === 'css' || label === 'scss' || label === 'less') {
      return '/css.worker.bundle.js';
    }
    if (label === 'html' || label === 'handlebars' || label === 'razor') {
      return '/html.worker.bundle.js';
    }
    if (label === 'typescript' || label === 'javascript') {
      return '/ts.worker.bundle.js';
    }
    return '/editor.worker.bundle.js';
  }
};

const Editor = React.forwardRef<
  monaco.editor.IStandaloneCodeEditor | undefined,
  any
>(({ width, height }, ref) => {
  const divEl = React.useRef<HTMLDivElement>(null);
  const [editor, setEditor] =
    React.useState<monaco.editor.IStandaloneCodeEditor>();

  React.useImperativeHandle(ref, () => editor);

  React.useEffect(() => {
    if (divEl.current) {
      const instance = monaco.editor.create(divEl.current, {
        value: ['// Loading...'].join('\n'),
        theme: 'vs-dark',
        language: 'javascript',
        tabSize: 2
      });

      setEditor(instance);
    }

    return () => {
      editor!.dispose();
    };
  }, []);

  React.useEffect(() => {
    editor?.layout();
  }, [width, height]);

  return <div ref={divEl} style={{ width, height }}></div>;
});

export default Editor;
