import * as React from 'react';
import Skeleton from '@mui/material/Skeleton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import {
  Link as RouterLink,
  LinkProps as RouterLinkProps
} from 'react-router-dom';
import HomeBlock from './HomeBlock';
import { CreateProjectDialog } from './CreateProjectDialog';

const Projects = () => {
  const [projects, setProjects] = React.useState<any[] | null>(null);
  const [open, setOpen] = React.useState(false);

  React.useEffect(() => {
    (async () => {
      const response = await fetch('/api/projects');
      const projects = await response.json();
      setProjects(projects);
    })();
  }, []);

  const handleAddClick = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const renderLink = React.useMemo(
    () => (id) =>
      React.forwardRef<HTMLAnchorElement, Omit<RouterLinkProps, 'to'>>(
        function Link(itemProps, ref) {
          return (
            <RouterLink
              to={`/projects/${id}`}
              ref={ref}
              {...itemProps}
              role={undefined}
            />
          );
        }
      ),
    []
  );

  const renderContent = () => {
    if (!projects) {
      return (
        <React.Fragment>
          <Skeleton animation="wave" />
          <Skeleton animation="wave" />
          <Skeleton animation="wave" />
        </React.Fragment>
      );
    }

    return (
      <List>
        {projects.map((project) => (
          <ListItem
            component={renderLink(project.id)}
            key={project.id}
            dense
            button
          >
            <ListItemText primary={project.name} />
          </ListItem>
        ))}
      </List>
    );
  };

  return (
    <React.Fragment>
      <CreateProjectDialog open={open} onClose={handleClose} />
      <HomeBlock title="Projects" onAddClick={handleAddClick}>
        {renderContent()}
      </HomeBlock>
    </React.Fragment>
  );
};

export default Projects;
