import * as React from 'react';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import CancelIcon from '@mui/icons-material/Close';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Autocomplete from '@mui/material/Autocomplete';

const getColor = (props) => {
  if (props.isDragAccept) {
    return '#00e676';
  }
  if (props.isDragReject) {
    return '#ff1744';
  }
  if (props.isDragActive) {
    return '#2196f3';
  }
  return '#eeeeee';
};

const Container = styled('div', {
  shouldForwardProp: (prop: string) =>
    !['isDragReject', 'isDragActive', 'isDragAccept'].includes(prop)
})`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-color: ${(props) => getColor(props)};
  border-style: dashed;
  background-color: #fafafa;
  color: #bdbdbd;
  outline: none;
  transition: border 0.24s ease-in-out;
`;

export const CreateProjectDialog = (props) => {
  const [name, setName] = React.useState();
  const [template, setTemplate] = React.useState();
  const [datasets, setDatasets] = React.useState<any>([]);
  const [templates, setTemplates] = React.useState<any>([]);
  const { onClose, open } = props;

  React.useEffect(() => {
    (async () => {
      const response = await fetch('/api/templates');
      const templates = await response.json();
      setTemplates(templates);
    })();
  }, []);

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleTemplateChange = (event, value) => {
    setTemplate(value.id);
  };

  const createProject = async () => {
    const data = JSON.stringify({ name, templateId: template });

    try {
      const result = await fetch('/api/projects/create', {
        method: 'POST',
        body: data,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      });
      if (!result.ok) {
        throw new Error('Something went wrong.');
      }
      const json = await result.json();
      setTimeout(() => {
        location.href = `/projects/${json.id}`;
      }, 1000);
    } catch (error: any) {
      alert(error.message);
    } finally {
      onClose();
    }
  };

  return (
    <Dialog maxWidth="sm" fullWidth open={open}>
      <DialogTitle
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}
      >
        <span>Create Project</span>
        <IconButton aria-label="close" edge="end" onClick={onClose}>
          <CancelIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <TextField
          label="Name"
          variant="outlined"
          onChange={handleNameChange}
          sx={{ mt: 2 }}
          autoFocus
          fullWidth
        />
        <Autocomplete
          options={templates}
          sx={{ mt: 2 }}
          getOptionLabel={(option) => option.fileName}
          onChange={handleTemplateChange}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="outlined"
              label="Template"
              fullWidth
            />
          )}
        />
        <Button
          type="button"
          onClick={createProject}
          sx={{ mt: 2, float: 'right' }}
        >
          Create
        </Button>
      </DialogContent>
    </Dialog>
  );
};
