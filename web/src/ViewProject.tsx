import * as React from 'react';
import * as ReactDOM from 'react-dom';
import moment from 'moment';
import { styled, alpha } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import PlayCircleFilledWhiteIcon from '@mui/icons-material/PlayCircleFilledWhite';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import InputBase from '@mui/material/InputBase';
import BuildIcon from '@mui/icons-material/Build';
import { AutoSizer } from 'react-virtualized';
import * as monaco from 'monaco-editor';
import { useParams } from 'react-router-dom';
import Editor from './Editor';
import RightSide from './RightSide';

const debounce = (callback) => {
  let timeout;
  return (...args) => {
    if (timeout) {
      clearTimeout(timeout);
      timeout = null;
    }
    timeout = setTimeout(() => {
      callback(...args);
    }, 500);
  };
};

const Spacer = styled('div')({
  flex: '1 0 auto'
});

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  ...theme.typography.h6,
  color: theme.palette.common.white,
  backgroundColor: alpha(theme.palette.common.white, 0.2),
  paddingLeft: theme.spacing(1)
}));

const Title = ({ name: nameProp, onChange }) => {
  const [editing, setEditing] = React.useState(false);
  const [name, setName] = React.useState(nameProp);
  const inputRef = React.useRef<HTMLInputElement>();

  const handleClick = () => {
    setEditing(true);
  };

  const handleChange = (event) => {
    setName(event.target.value);
  };

  const handleBlur = () => {
    setEditing(false);
    onChange(name);
  };

  const handleKeyDown = (event) => {
    if (event.key === 'Enter' || event.key === 'Escape') {
      setEditing(false);
      onChange(name);
    }
  };

  React.useEffect(() => {
    if (editing) {
      inputRef.current!.focus();
    }
  }, [editing]);

  if (editing) {
    return (
      <StyledInputBase
        value={name}
        inputRef={inputRef}
        onChange={handleChange}
        onBlur={handleBlur}
        onKeyDown={handleKeyDown}
      />
    );
  }

  return (
    <Typography variant="h6" onClick={handleClick}>
      {name}
    </Typography>
  );
};

const ViewProject = () => {
  const { id } = useParams<{ id: string }>();
  const [templates, setTemplates] = React.useState<any>();
  const [datasets, setDatasets] = React.useState<any>();
  const [projectInfo, setProjectInfo] = React.useState<any>(null);
  const lastSaved = React.useRef(null);
  const [lastUpdate, setLastUpdate] = React.useState(null);
  const [editor, setEditor] =
    React.useState<monaco.editor.IStandaloneCodeEditor | null>(null);

  React.useEffect(() => {
    (async () => {
      const response = await fetch(`/api/projects/${id}/info`);
      const info = await response.json();
      setProjectInfo(info);
      setLastUpdate(info.lastUpdate);
      lastSaved.current = info;
    })();
  }, [id]);

  React.useEffect(() => {
    (async () => {
      const response = await fetch('/api/templates');
      const templates = await response.json();
      setTemplates(templates);
    })();
  }, []);

  React.useEffect(() => {
    (async () => {
      const response = await fetch('/api/datasets');
      const datasets = await response.json();
      setDatasets(datasets);
    })();
  }, []);

  React.useEffect(() => {
    if (!projectInfo || projectInfo === lastSaved.current) {
      return;
    }

    (async () => {
      const response = await fetch(`/api/projects/${id}/info`, {
        body: JSON.stringify(projectInfo),
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      const json = await response.json();
      lastSaved.current = json;
      setProjectInfo(json);
    })();
  }, [id, projectInfo]);

  const saveCode = React.useMemo(
    () =>
      debounce(async () => {
        const response = await fetch(`/api/projects/${id}`, {
          body: editor!.getValue(),
          method: 'PUT'
        });
        const json = await response.json();
        setLastUpdate(json.lastUpdate);
      }),
    [id, editor]
  );

  const triggerJob = async () => {
    if (!confirm('Are you sure?')) {
      return;
    }
    try {
      const result = await fetch(`/api/jobs/create/${id}`, {
        method: 'POST'
      });
      if (!result.ok) {
        throw new Error('Something went wrong.');
      }
      const json = await result.json();
      alert(`Job ${json.id} triggered successfully.`);
      location.href = `/`;
    } catch (error: any) {
      alert(error.message);
    }
  };

  const regenerate = async () => {
    const response = await fetch(`/api/projects/${id}/regenerate`, {
      body: JSON.stringify({ templateId: projectInfo.templateId }),
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const json = await response.json();
    setLastUpdate(json.lastUpdate);
    loadSavedCode();
  };

  const loadSavedCode = React.useCallback(async () => {
    const response = await fetch(`/api/projects/${id}`);
    const project = await response.text();
    editor!.setValue(project);
  }, [editor, id]);

  React.useEffect(() => {
    if (!id || !editor) {
      return;
    }
    loadSavedCode();
  }, [id, editor, loadSavedCode]);

  React.useEffect(() => {
    if (!id || !editor) {
      return;
    }
    editor.onDidChangeModelContent(saveCode);
  }, [id, editor]);

  const handleEditorRef = (el) => {
    if (el) {
      setEditor(el);
    }
  };

  const handleNameChange = (newName) => {
    setProjectInfo({ ...projectInfo, name: newName });
  };

  const handleDatasetChange = (event, value) => {
    setProjectInfo({ ...projectInfo, datasetId: value.id });
  };

  if (!projectInfo || !datasets || !templates) {
    return <CircularProgress />;
  }

  return (
    <Stack sx={{ flex: '1 1 auto', overflow: 'hidden', bgcolor: '#fff' }}>
      {ReactDOM.createPortal(
        <Title name={projectInfo.name} onChange={handleNameChange} />,
        document.getElementById('title')!
      )}
      <Box
        sx={{
          p: 2,
          display: 'flex',
          alignItems: 'center',
          borderBottom: '1px solid #ccc'
        }}
      >
        <Autocomplete
          disablePortal
          options={templates}
          value={{
            id: projectInfo.templateId,
            name: templates.find((t) => t.id === projectInfo.templateId).name
          }}
          isOptionEqualToValue={(option, value) => option.id === value.id}
          sx={{ width: 300 }}
          getOptionLabel={(option) => option.id}
          disableClearable
          renderInput={(params) => (
            <TextField {...params} size="small" label="Template" />
          )}
        />
        <Tooltip title="Re-generate from template">
          <IconButton onClick={regenerate} sx={{ ml: 1 }}>
            <BuildIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Trigger job">
          <IconButton onClick={triggerJob} sx={{ ml: 1 }}>
            <PlayCircleFilledWhiteIcon />
          </IconButton>
        </Tooltip>
        <Spacer />
        {lastUpdate && (
          <Typography color="text.secondary">
            Last updated {moment(lastUpdate).format('DD/MM/YYYY HH:mm:ss')}
          </Typography>
        )}
      </Box>
      <Box sx={{ display: 'flex', overflow: 'hidden', flex: '1 1 auto' }}>
        <Box sx={{ flex: 1 }}>
          <AutoSizer>
            {({ width, height }) => (
              <Editor ref={handleEditorRef} width={width} height={height} />
            )}
          </AutoSizer>
        </Box>
        <Box sx={{ flex: 1 }}>
          <RightSide
            datasets={datasets}
            projectId={id}
            dataset={projectInfo.datasetId}
            onDatasetChange={handleDatasetChange}
          />
        </Box>
      </Box>
    </Stack>
  );
};

export default ViewProject;
