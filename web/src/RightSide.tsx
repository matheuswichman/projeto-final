import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Skeleton from '@mui/material/Skeleton';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import { DataGridPro, useGridApiRef } from '@mui/x-data-grid-pro';
import { grey } from '@mui/material/colors';
import { styled } from '@mui/material/styles';

const StyledTab = styled(Tab)(({ theme }) => ({
  '&.Mui-selected': {
    backgroundColor: '#fff',
    boxShadow: theme.shadows[1]
  }
}));

const RightSide = ({ projectId, datasets, dataset, onDatasetChange }) => {
  const [tab, setTab] = React.useState('preview');
  const [rows, setRows] = React.useState<any>(null);
  const [clickedRow, setClickedRow] = React.useState<any>(null);
  const apiRef = useGridApiRef();

  const loadDataset = React.useCallback(async () => {
    if (!dataset) {
      return;
    }
    const response = await fetch(`/api/datasets/${dataset}`);
    const results = await response.json();
    setRows(results);
  }, [dataset]);

  React.useEffect(() => {
    loadDataset();
  }, [loadDataset]);

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setTab(newValue);
  };

  const [contextMenu, setContextMenu] = React.useState<{
    mouseX: number;
    mouseY: number;
  } | null>(null);

  const handleContextMenu = (event: React.MouseEvent) => {
    event.preventDefault();
    setContextMenu(
      contextMenu === null
        ? { mouseX: event.clientX - 2, mouseY: event.clientY - 4 }
        : null
    );
    setClickedRow(Number(event.currentTarget.getAttribute('data-id')));
  };

  const handleClose = () => {
    setContextMenu(null);
  };

  const generatePreview = async () => {
    const row = rows.find((row) => row.id === clickedRow);
    if (!row) {
      return;
    }
    const data = JSON.stringify(row);
    const w = window.open(
      `/api/projects/${projectId}/preview?data=${btoa(data)}`,
      '_blank'
    );
    w!.focus();
  };

  if (!dataset) {
    return (
      <Box sx={{ bgcolor: '#eee', overflow: 'hidden', height: 1 }}>
        <Paper sx={{ width: '80%', margin: '40px auto', p: 3 }}>
          <Typography
            variant="body1"
            sx={{ mt: 2, textAlign: 'center' }}
            color="textSecondary"
          >
            You have to select a dataset first.
          </Typography>
          <Autocomplete
            options={datasets}
            onChange={onDatasetChange}
            sx={{ mt: 4 }}
            getOptionLabel={(option: any) => option.name}
            disableClearable
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                size="small"
                label="Dataset"
                fullWidth
              />
            )}
          />
        </Paper>
      </Box>
    );
  }

  if (!datasets || !rows) {
    return (
      <Box sx={{ m: 1 }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
      </Box>
    );
  }

  const currentDataset = datasets.find(
    (otherDataset) => otherDataset.id === dataset
  );

  const columns = currentDataset.columns.sort();

  const gridColumns = [
    { field: 'id', headerName: 'Row #' },
    ...columns.map((column) => ({ field: column }))
  ];

  return (
    <Stack sx={{ width: '100%', height: '100%' }}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          bgcolor: grey[100]
        }}
      >
        <Tabs
          value={tab}
          onChange={handleChange}
          aria-label="basic tabs example"
          sx={{ flex: '1 0 auto' }}
        >
          <StyledTab value="preview" label="Preview" />
          <StyledTab value="structure" label="Structure" />
        </Tabs>
        <Autocomplete
          options={datasets}
          value={currentDataset}
          onChange={onDatasetChange}
          sx={{ width: 300 }}
          getOptionLabel={(option: any) => option.name}
          disableClearable
          renderInput={(params) => (
            <TextField
              {...params}
              variant="standard"
              size="small"
              label="Dataset"
            />
          )}
        />
      </Box>
      <Box
        sx={{
          display: tab === 'structure' ? 'block' : 'none',
          flex: '1 1 auto',
          overflow: 'auto'
        }}
      >
        {columns.sort().map((column, index) => (
          <Chip
            key={index}
            label={column}
            size="small"
            variant="outlined"
            sx={{ m: 0.5 }}
          />
        ))}
      </Box>
      <Box
        sx={{
          display: tab === 'preview' ? 'block' : 'none',
          flex: '1 1 auto',
          overflow: 'auto'
        }}
      >
        <DataGridPro
          apiRef={apiRef}
          density="compact"
          columns={gridColumns}
          disableMultipleSelection
          rows={rows}
          componentsProps={{
            row: {
              onContextMenu: handleContextMenu,
              style: { cursor: 'context-menu' }
            }
          }}
        />
        <Menu
          open={contextMenu !== null}
          onClose={handleClose}
          anchorReference="anchorPosition"
          anchorPosition={
            contextMenu !== null
              ? { top: contextMenu.mouseY, left: contextMenu.mouseX }
              : undefined
          }
          componentsProps={{
            root: {
              onContextMenu: (e) => {
                e.preventDefault();
                handleClose();
              }
            }
          }}
        >
          <MenuItem onClick={generatePreview}>Preview conversion</MenuItem>
        </Menu>
      </Box>
    </Stack>
  );
};

export default RightSide;
