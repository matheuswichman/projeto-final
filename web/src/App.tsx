import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { LicenseInfo } from '@mui/x-data-grid-pro';
import AppBar from '@mui/material/AppBar';
import Stack from '@mui/material/Stack';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import ViewProject from './ViewProject';
import Home from './Home';

LicenseInfo.setLicenseKey(
  '0f94d8b65161817ca5d7f7af8ac2f042T1JERVI6TVVJLVN0b3J5Ym9vayxFWFBJUlk9MTY1NDg1ODc1MzU1MCxLRVlWRVJTSU9OPTE='
);

const theme = createTheme({
  palette: {
    background: {
      default: '#eee'
    }
  },
  shape: {
    borderRadius: 0
  }
});

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Stack sx={{ height: '100vh' }}>
        <CssBaseline />
        <Router>
          <AppBar position="relative">
            <Toolbar>
              <Typography
                component={Link}
                variant="h6"
                sx={{
                  mr: 2,
                  fontWeight: 'bold',
                  opacity: 0.8,
                  textDecoration: 'none',
                  color: '#fff'
                }}
                to="/"
              >
                EHR2ANY
              </Typography>
              <div id="title" />
            </Toolbar>
          </AppBar>
          <Switch>
            <Route path="/projects/:id">
              <ViewProject />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
      </Stack>
    </ThemeProvider>
  );
};

export default App;
