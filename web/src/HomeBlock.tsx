import * as React from 'react';
import AddIcon from '@mui/icons-material/Add';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';

const HomeBlock = ({ children, onAddClick, title, ...other }: any) => {
  return (
    <Card sx={{ height: 250, display: 'flex', flexDirection: 'column' }}>
      <Box
        sx={{
          p: 2,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          borderBottom: 1,
          borderColor: 'divider',
          minHeight: 73
        }}
        {...other}
      >
        <Typography variant="h6">{title}</Typography>
        {onAddClick && (
          <IconButton color="primary" onClick={onAddClick}>
            <AddIcon />
          </IconButton>
        )}
      </Box>
      <Box sx={{ p: 2, overflow: 'auto' }}>{children}</Box>
    </Card>
  );
};

export default HomeBlock;
