import * as React from 'react';
import Skeleton from '@mui/material/Skeleton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import Box from '@mui/material/Box';
import HomeBlock from './HomeBlock';

const Jobs = () => {
  const [jobs, setJobs] = React.useState<any[] | null>(null);
  const intervalRef = React.useRef<any>(null);

  const refreshJobs = React.useCallback(async () => {
    const response = await fetch('/api/jobs');
    const jobs = await response.json();
    setJobs(jobs);
  }, []);

  React.useEffect(() => {
    refreshJobs();

    intervalRef.current = setInterval(() => {
      refreshJobs();
    }, 1000);

    return () => {
      clearInterval(intervalRef.current);
    };
  }, [refreshJobs]);

  const openJobFolder = (id) => () => {
    const w = window.open(`/filemanager/browse/${id}`, '_blank');
    w.focus();
  };

  const renderContent = () => {
    if (!jobs) {
      return (
        <React.Fragment>
          <Skeleton animation="wave" />
          <Skeleton animation="wave" />
          <Skeleton animation="wave" />
        </React.Fragment>
      );
    }

    return (
      <List>
        {jobs.map((project) => (
          <ListItem
            key={project.id}
            onClick={openJobFolder(project.id)}
            disablePadding
            dense
          >
            <ListItemButton>
              <ListItemText
                primary={project.id}
                secondary={`${project.convertedRows} rows converted of ${project.totalRows} rows`}
              />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    );
  };

  return <HomeBlock title="Jobs">{renderContent()}</HomeBlock>;
};

export default Jobs;
