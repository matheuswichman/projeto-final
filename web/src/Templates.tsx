import * as React from 'react';
import Skeleton from '@mui/material/Skeleton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import HomeBlock from './HomeBlock';
import { UploadDialog } from './UploadDialog';

const Templates = () => {
  const [templates, setTemplates] = React.useState<any[] | null>(null);
  const [open, setOpen] = React.useState(false);

  const load = React.useCallback(async () => {
    const response = await fetch('/api/templates');
    const templates = await response.json();
    setTemplates(templates);
  }, []);

  React.useEffect(() => {
    load();
  }, [load]);

  const handleAddClick = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleFileSelected = async (file) => {
    const data = new FormData();
    data.append('template', file);

    try {
      const result = await fetch('/api/templates/upload', {
        method: 'POST',
        body: data
      });
      if (!result.ok) {
        throw new Error('Something went wrong.');
      }
      load();
    } catch (error: any) {
      alert(error.message);
    } finally {
      setOpen(false);
    }
  };

  const renderContent = () => {
    if (!templates) {
      return (
        <React.Fragment>
          <Skeleton animation="wave" />
          <Skeleton animation="wave" />
          <Skeleton animation="wave" />
        </React.Fragment>
      );
    }

    return (
      <List>
        {templates.map((dataset) => (
          <ListItem key={dataset.id} disablePadding dense>
            <ListItemButton>
              <ListItemText
                primary={dataset.name}
                secondary={dataset.fileName}
              />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    );
  };

  return (
    <React.Fragment>
      <UploadDialog
        title="Upload template"
        open={open}
        onClose={handleClose}
        onFileSelected={handleFileSelected}
      />
      <HomeBlock title="Templates" onAddClick={handleAddClick}>
        {renderContent()}
      </HomeBlock>
    </React.Fragment>
  );
};

export default Templates;
