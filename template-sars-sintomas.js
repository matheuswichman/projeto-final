function generate(data) {
  const sintomas = [];
  [
    'FEBRE',
    'TOSSE',
    'GARGANTA',
    'DISPNEIA',
    'DESC_RESP',
    'SATURACAO',
    'DIARREIA',
    'VOMITO',
    'DOR_ABD',
    'FADIGA',
    'PERD_OLFT',
    'PERD_PALA'
  ].forEach((field) => {
    if (!data[field]) {
      return;
    }
    let code = 'at0027';
    if (data[field] === '1') {
      code = 'at0023';
    } else if (data[field] === '2') {
      code = 'at0024';
    }
    sintomas.push({ name: field, presence: code });
  });

  return COMPOSITION(/* Encounter [1..1] -- / */ 'at0000', {
    archetypeId: 'openEHR-EHR-COMPOSITION.encounter.v1',
    templateId: 'SARS event notification',
    language: CODE_PHRASE({ value: 'enUS' }),
    territory: CODE_PHRASE({ value: 'BRA' }),
    category /* [1..1] */: DV_CODED_TEXT({
      definingCode: CODE_PHRASE({
        codeString: '433',
        terminologyId: 'openehr'
      })
    }),
    content /* [0..1] */: [
      OBSERVATION(
        /* Symptom/sign screening questionnaire [0..*] -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-OBSERVATION.symptom_sign_screening.v0',
          data /* [1..1] */: HISTORY(
            /* History [1..1] -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001] */ 'at0001',
            {
              data: ITEM_TREE(
                /* Tree [1..1] -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003] */ 'at0003',
                {
                  items /* [0..1] */: [
                    ELEMENT(
                      /* Presence of any symptoms or signs? [0..1] -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0028] */ 'at0028',
                      {
                        value /* [0..1] */: DV_TEXT({
                          value: $('DT_NOTIFIC')
                        })
                      }
                    ),
                    ELEMENT(
                      /* Onset of any symptoms or signs [0..1] -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0029] */ 'at0029',
                      {
                        value /* [0..1] */: DV_DATE_TIME({
                          value: $('DT_SIN_PRI')
                        })
                      }
                    ),
                    ...sintomas.map(({ name, presence }) => {
                      return CLUSTER(
                        /* Specific symptom/sign [0..*] -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0022] */ 'at0022',
                        {
                          items /* [1..1] */: [
                            ELEMENT(
                              /* Symptom or sign name [1..1] -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0022]/items[at0004] */ 'at0004',
                              { value /* [0..1] */: DV_TEXT({ value: name }) }
                            ),
                            ELEMENT(
                              /* Presence? [0..1] -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0022]/items[at0005] */ 'at0005',
                              {
                                value /* [0..1] */: DV_CODED_TEXT({
                                  value: presence,
                                  definingCode: CODE_PHRASE({
                                    terminologyId: 'custom',
                                    codeString: presence
                                  })
                                })
                              }
                            )
                          ]
                        }
                      );
                    })
                  ]
                }
              )
            }
          )
        }
      )
    ]
  });
}
module.exports = generate;
