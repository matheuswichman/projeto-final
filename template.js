function generate(data) {
  return COMPOSITION(/* Encounter  [1..1]  -- / */ 'at0000', {
    archetypeId: 'openEHR-EHR-COMPOSITION.encounter.v1',
    templateId: 'SARS event notification',
    category /* [1..1] */: DV_CODED_TEXT({
      definingCode: CODE_PHRASE({ codeString: '433', terminologyId: 'openehr' })
    }),
    context /* [0..1] */: EVENT_CONTEXT({
      other_context /* [0..1] */: ITEM_TREE(
        /* Tree  [1..1]  -- /context/other_context[at0001] */ 'at0001',
        {
          items /* [0..1] */: [
            CLUSTER(
              /* Organisation  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.organisation.v0] */ 'at0000',
              {
                archetypeId: 'openEHR-EHR-CLUSTER.organisation.v0',
                items /* [1..1] */: [
                  ELEMENT(
                    /* Organisation name  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.organisation.v0]/items[at0001] */ 'at0001',
                    { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                  ),
                  ELEMENT(
                    /* Identifier  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.organisation.v0]/items[at0011] */ 'at0011',
                    { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                  ),
                  CLUSTER(
                    /* Address details  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.organisation.v0]/items[at0008] */ 'at0008',
                    {
                      archetypeId: '?' // /openEHR-EHR-CLUSTER\.address\.v1|openEHR-EHR-CLUSTER\.address\.v0/
                    }
                  ),
                  CLUSTER(
                    /* Telecom details  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.organisation.v0]/items[at0009] */ 'at0009',
                    {
                      archetypeId: '?' // /openEHR-EHR-CLUSTER\.telecom_details\.v1|openEHR-EHR-CLUSTER\.telecom_details\.v0/
                    }
                  ),
                  CLUSTER(
                    /* Contact details  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.organisation.v0]/items[at0005] */ 'at0005',
                    {
                      items /* [1..1] */: [
                        CLUSTER(
                          /* Contact name  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.organisation.v0]/items[at0005]/items[at0010] */ 'at0010',
                          {
                            archetypeId: '?' // /openEHR-EHR-CLUSTER\.person_name\.v1|openEHR-EHR-CLUSTER\.person_name\.v0/
                          }
                        ),
                        ELEMENT(
                          /* Role in organisation  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.organisation.v0]/items[at0005]/items[at0007] */ 'at0007',
                          { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                        )
                      ]
                    }
                  )
                ]
              }
            ),
            CLUSTER(
              /* Individual's personal demographics  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0] */ 'at0000',
              {
                archetypeId: 'openEHR-EHR-CLUSTER.individual_personal.v0',
                items /* [1..1] */: [
                  CLUSTER(
                    /* Person name  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0] */ 'at0000',
                    {
                      archetypeId: 'openEHR-EHR-CLUSTER.person_name.v0',
                      items /* [1..1] */: [
                        ELEMENT(
                          /* Name type  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0006] */ 'at0006',
                          {
                            value /* [0..1] */: DV_CODED_TEXT({
                              definingCode: CODE_PHRASE({
                                codeString:
                                  'at0020|at0008|at0009|at0010|at0011|at0012|at0019|at0021',
                                terminologyId: 'local'
                              })
                            })
                          }
                        ),
                        ELEMENT(
                          /* Preferred name  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0022] */ 'at0022',
                          { value /* [0..1] */: DV_BOOLEAN({ value: '?' }) }
                        ),
                        ELEMENT(
                          /* Unstructured name  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0001] */ 'at0001',
                          { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                        ),
                        CLUSTER(
                          /* Structured name  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0002] */ 'at0002',
                          {
                            items /* [1..1] */: [
                              ELEMENT(
                                /* Title  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0002]/items[at0017] */ 'at0017',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              ),
                              ELEMENT(
                                /* Given name  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0002]/items[at0003] */ 'at0003',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              ),
                              ELEMENT(
                                /* Middle name  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0002]/items[at0004] */ 'at0004',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              ),
                              ELEMENT(
                                /* Family name  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0002]/items[at0005] */ 'at0005',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              ),
                              ELEMENT(
                                /* Suffix  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0002]/items[at0018] */ 'at0018',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              )
                            ]
                          }
                        ),
                        ELEMENT(
                          /* Validity period  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.person_name.v0]/items[at0014] */ 'at0014',
                          { value /* [0..1] */: DV_INTERVAL({ value: '?' }) }
                        )
                      ]
                    }
                  ),
                  ELEMENT(
                    /* Identifier  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[at0016] */ 'at0016',
                    { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                  ),
                  ELEMENT(
                    /* Date of Birth  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[at0007] */ 'at0007',
                    { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                  ),
                  ELEMENT(
                    /* Relationship to subject  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[at0008] */ 'at0008',
                    { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                  ),
                  CLUSTER(
                    /* Address  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0] */ 'at0000',
                    {
                      archetypeId: 'openEHR-EHR-CLUSTER.address.v0',
                      items /* [1..1] */: [
                        CLUSTER(
                          /* Address  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001] */ 'at0001',
                          {
                            items /* [1..1] */: [
                              ELEMENT(
                                /* Address Type  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001]/items[at0006] */ 'at0006',
                                {
                                  value /* [0..1] */: DV_CODED_TEXT({
                                    definingCode: CODE_PHRASE({
                                      codeString: 'at0011|at0012|at0013|at0014',
                                      terminologyId: 'local'
                                    })
                                  })
                                }
                              ),
                              ELEMENT(
                                /* Unstructured address  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001]/items[at0002] */ 'at0002',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              ),
                              CLUSTER(
                                /* Structured address  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001]/items[at0003] */ 'at0003',
                                {
                                  items /* [1..1] */: [
                                    ELEMENT(
                                      /* Property number  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001]/items[at0003]/items[at0005] */ 'at0005',
                                      {
                                        value /* [0..1] */: DV_TEXT({
                                          value: '?'
                                        })
                                      }
                                    ),
                                    ELEMENT(
                                      /* Address line  [0..4]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001]/items[at0003]/items[at0009] */ 'at0009',
                                      {
                                        value /* [0..1] */: DV_TEXT({
                                          value: '?'
                                        })
                                      }
                                    )
                                  ]
                                }
                              ),
                              ELEMENT(
                                /* Post code  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001]/items[at0004] */ 'at0004',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              ),
                              CLUSTER(
                                /* AddressValid Period  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001]/items[at0015] */ 'at0015',
                                {
                                  items /* [1..1] */: [
                                    ELEMENT(
                                      /* Valid from  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001]/items[at0015]/items[at0007] */ 'at0007',
                                      {
                                        value /* [0..1] */: DV_DATE_TIME({
                                          value: '?'
                                        })
                                      }
                                    ),
                                    ELEMENT(
                                      /* Valid to  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.address.v0]/items[at0001]/items[at0015]/items[at0008] */ 'at0008',
                                      {
                                        value /* [0..1] */: DV_DATE_TIME({
                                          value: '?'
                                        })
                                      }
                                    )
                                  ]
                                }
                              )
                            ]
                          }
                        )
                      ]
                    }
                  ),
                  CLUSTER(
                    /* Communication details  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0] */ 'at0000',
                    {
                      archetypeId: 'openEHR-EHR-CLUSTER.telecom_details.v0',
                      items /* [1..1] */: [
                        ELEMENT(
                          /* Mode  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0010] */ 'at0010',
                          {
                            value /* [0..1] */: DV_CODED_TEXT({
                              definingCode: CODE_PHRASE({
                                codeString: 'at0011|at0012',
                                terminologyId: 'local'
                              })
                            })
                          }
                        ),
                        CLUSTER(
                          /* Telecoms  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0001] */ 'at0001',
                          {
                            items /* [1..1] */: [
                              ELEMENT(
                                /* Telecoms type  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0001]/items[at0004] */ 'at0004',
                                {
                                  value /* [0..1] */: DV_CODED_TEXT({
                                    definingCode: CODE_PHRASE({
                                      codeString: 'at0013|at0014|at0015|at0016',
                                      terminologyId: 'local'
                                    })
                                  })
                                }
                              ),
                              ELEMENT(
                                /* Unstructured telecoms  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0001]/items[at0002] */ 'at0002',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              ),
                              CLUSTER(
                                /* Structured telecoms  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0001]/items[at0003] */ 'at0003',
                                {
                                  items /* [1..1] */: [
                                    ELEMENT(
                                      /* Country code  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0001]/items[at0003]/items[at0005] */ 'at0005',
                                      {
                                        value /* [0..1] */: DV_TEXT({
                                          value: '?'
                                        })
                                      }
                                    ),
                                    ELEMENT(
                                      /* Area code  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0001]/items[at0003]/items[at0006] */ 'at0006',
                                      {
                                        value /* [0..1] */: DV_TEXT({
                                          value: '?'
                                        })
                                      }
                                    ),
                                    ELEMENT(
                                      /* Number  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0001]/items[at0003]/items[at0007] */ 'at0007',
                                      {
                                        value /* [0..1] */: DV_TEXT({
                                          value: '?'
                                        })
                                      }
                                    ),
                                    ELEMENT(
                                      /* Extension  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0001]/items[at0003]/items[at0019] */ 'at0019',
                                      {
                                        value /* [0..1] */: DV_TEXT({
                                          value: '?'
                                        })
                                      }
                                    )
                                  ]
                                }
                              )
                            ]
                          }
                        ),
                        CLUSTER(
                          /* Internet communication  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0020] */ 'at0020',
                          {
                            items /* [1..1] */: [
                              ELEMENT(
                                /* Channel  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0020]/items[at0021] */ 'at0021',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              ),
                              ELEMENT(
                                /* Address  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-EHR-CLUSTER.telecom_details.v0]/items[at0020]/items[at0009] */ 'at0009',
                                { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                              )
                            ]
                          }
                        )
                      ]
                    }
                  ),
                  CLUSTER(
                    /* Ethnicity/Indigenous status  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[at0018] */ 'at0018',
                    {
                      archetypeId: '?' // /.*/
                    }
                  ),
                  CLUSTER(
                    /* Person additional demographic data  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-DEMOGRAPHIC-CLUSTER.person_additional_data_iso.v0] */ 'at0000',
                    {
                      archetypeId:
                        'openEHR-DEMOGRAPHIC-CLUSTER.person_additional_data_iso.v0',
                      items /* [1..1] */: [
                        ELEMENT(
                          /* Sex  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-DEMOGRAPHIC-CLUSTER.person_additional_data_iso.v0]/items[at0001] */ 'at0001',
                          {
                            value /* [0..1] */: DV_CODED_TEXT({
                              definingCode: CODE_PHRASE({
                                codeString: 'at0010|at0011|at0012|at0013',
                                terminologyId: 'local'
                              })
                            })
                          }
                        ),
                        ELEMENT(
                          /* Mother's family name  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-DEMOGRAPHIC-CLUSTER.person_additional_data_iso.v0]/items[at0002] */ 'at0002',
                          { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                        ),
                        ELEMENT(
                          /* Identification comment  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.individual_personal.v0]/items[openEHR-DEMOGRAPHIC-CLUSTER.person_additional_data_iso.v0]/items[at0003] */ 'at0003',
                          { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                        )
                      ]
                    }
                  )
                ]
              }
            ),
            CLUSTER(
              /* Healthcare professional (PARENT)  [1..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.healthcare_professional_parent.v1] */ 'at0000',
              {
                archetypeId:
                  'openEHR-EHR-CLUSTER.healthcare_professional_parent.v1',
                items /* [1..1] */: [
                  CLUSTER(
                    /* Professional Name  [0..1]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.healthcare_professional_parent.v1]/items[at0001] */ 'at0001',
                    {
                      archetypeId: '?' // /.*/
                    }
                  ),
                  ELEMENT(
                    /* Professional Identifier  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.healthcare_professional_parent.v1]/items[at0002] */ 'at0002',
                    { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                  ),
                  CLUSTER(
                    /* Provider Organisation  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.healthcare_professional_parent.v1]/items[at0003] */ 'at0003',
                    {
                      archetypeId: '?' // /.*/
                    }
                  ),
                  ELEMENT(
                    /* Contact details  [0..*]  -- /context/other_context[at0001]/items[openEHR-EHR-CLUSTER.healthcare_professional_parent.v1]/items[at0004] */ 'at0004',
                    { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                  )
                ]
              }
            )
          ]
        }
      )
    }),
    content /* [0..1] */: [
      ADMIN_ENTRY(
        /* Episode of care - institution  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.episode_institution.v0] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-ADMIN_ENTRY.episode_institution.v0',
          data /* [1..1] */: ITEM_TREE(
            /* Item tree  [1..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.episode_institution.v0]/data[at0001] */ 'at0001',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Admission date  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.episode_institution.v0]/data[at0001]/items[at0004] */ 'at0004',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Admitted from  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.episode_institution.v0]/data[at0001]/items[at0007] */ 'at0007',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Separation date  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.episode_institution.v0]/data[at0001]/items[at0002] */ 'at0002',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Outcome  [0..*]  -- /content[openEHR-EHR-ADMIN_ENTRY.episode_institution.v0]/data[at0001]/items[at0006] */ 'at0006',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Destination category  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.episode_institution.v0]/data[at0001]/items[at0003] */ 'at0003',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                )
              ]
            }
          )
        }
      ),
      EVALUATION(
        /* Education summary  [0..*]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-EVALUATION.education_summary.v1',
          data /* [1..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/data[at0001] */ 'at0001',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Description  [0..1]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/data[at0001]/items[at0018] */ 'at0018',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Age started  [0..1]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/data[at0001]/items[at0003] */ 'at0003',
                  { value /* [0..1] */: DV_DURATION({ value: '?' }) }
                ),
                ELEMENT(
                  /* Age ended  [0..1]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/data[at0001]/items[at0031] */ 'at0031',
                  { value /* [0..1] */: DV_DURATION({ value: '?' }) }
                ),
                ELEMENT(
                  /* Highest level completed  [0..1]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/data[at0001]/items[at0002] */ 'at0002',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Education record  [0..*]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/data[at0001]/items[at0029] */ 'at0029',
                  {
                    archetypeId: '?' // /.*/
                  }
                ),
                CLUSTER(
                  /* Additional details  [0..*]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/data[at0001]/items[at0030] */ 'at0030',
                  {
                    archetypeId: '?' // /.*/
                  }
                ),
                ELEMENT(
                  /* Comment  [0..1]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/data[at0001]/items[at0007] */ 'at0007',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                )
              ]
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/protocol[at0026] */ 'at0026',
            {
              items /* [0..1] */: [
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/protocol[at0026]/items[at0027] */ 'at0027',
                  {
                    archetypeId: '?' // /.*/
                  }
                ),
                ELEMENT(
                  /* Last updated  [0..1]  -- /content[openEHR-EHR-EVALUATION.education_summary.v1]/protocol[at0026]/items[at0028] */ 'at0028',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                )
              ]
            }
          )
        }
      ),
      OBSERVATION(
        /* Pregnancy status  [0..1]  -- /content[openEHR-EHR-OBSERVATION.pregnancy_status.v0] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-OBSERVATION.pregnancy_status.v0',
          data /* [1..1] */: HISTORY(
            /* History  [1..1]  -- /content[openEHR-EHR-OBSERVATION.pregnancy_status.v0]/data[at0001] */ 'at0001',
            {
              data: ITEM_TREE(
                /* Tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.pregnancy_status.v0]/data[at0001]/events[at0002]/data[at0003] */ 'at0003',
                {
                  items /* [0..1] */: [
                    ELEMENT(
                      /* Status  [1..1]  -- /content[openEHR-EHR-OBSERVATION.pregnancy_status.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0011] */ 'at0011',
                      {
                        value /* [0..1] */: DV_CODED_TEXT({
                          definingCode: CODE_PHRASE({
                            codeString: 'at0012|at0024|at0013|at0014',
                            terminologyId: 'local'
                          })
                        })
                      }
                    ),
                    ELEMENT(
                      /* Evidence  [0..*]  -- /content[openEHR-EHR-OBSERVATION.pregnancy_status.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0015] */ 'at0015',
                      {
                        value /* [0..1] */: DV_CODED_TEXT({
                          definingCode: CODE_PHRASE({
                            codeString: 'at0016|at0017|at0018|at0019|at0020',
                            terminologyId: 'local'
                          })
                        })
                      }
                    ),
                    ELEMENT(
                      /* Comment  [0..1]  -- /content[openEHR-EHR-OBSERVATION.pregnancy_status.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0010] */ 'at0010',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    )
                  ]
                }
              )
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Item tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.pregnancy_status.v0]/protocol[at0021] */ 'at0021',
            {
              items /* [0..1] */: [
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-OBSERVATION.pregnancy_status.v0]/protocol[at0021]/items[at0022] */ 'at0022',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      OBSERVATION(
        /* Symptom/sign screening questionnaire  [0..*]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-OBSERVATION.symptom_sign_screening.v0',
          data /* [1..1] */: HISTORY(
            /* History  [1..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001] */ 'at0001',
            {
              data: ITEM_TREE(
                /* Tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003] */ 'at0003',
                {
                  items /* [0..1] */: [
                    ELEMENT(
                      /* Screening purpose  [0..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0034] */ 'at0034',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Presence of any symptoms or signs?  [0..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0028] */ 'at0028',
                      {
                        value /* [0..1] */: DV_CODED_TEXT({
                          definingCode: CODE_PHRASE({
                            codeString: 'at0031|at0032|at0033',
                            terminologyId: 'local'
                          })
                        })
                      }
                    ),
                    ELEMENT(
                      /* Onset of any symptoms or signs  [0..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0029] */ 'at0029',
                      { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Description  [0..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0036] */ 'at0036',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    CLUSTER(
                      /* Specific symptom/sign  [0..*]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0022] */ 'at0022',
                      {
                        items /* [1..1] */: [
                          ELEMENT(
                            /* Symptom or sign name  [1..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0022]/items[at0004] */ 'at0004',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Presence?  [0..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0022]/items[at0005] */ 'at0005',
                            {
                              value /* [0..1] */: DV_CODED_TEXT({
                                definingCode: CODE_PHRASE({
                                  codeString: 'at0023|at0024|at0027',
                                  terminologyId: 'local'
                                })
                              })
                            }
                          ),
                          CLUSTER(
                            /* Symptom/sign details  [0..*]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0022]/items[at0026] */ 'at0026',
                            {
                              archetypeId: '?' // /openEHR-EHR-CLUSTER\.symptom_sign(-[a-zA-Z0-9_]+)*\.v1/
                            }
                          ),
                          ELEMENT(
                            /* Comment  [0..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0022]/items[at0035] */ 'at0035',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          )
                        ]
                      }
                    ),
                    ELEMENT(
                      /* Overall comment  [0..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0025] */ 'at0025',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    )
                  ]
                }
              )
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Item tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/protocol[at0007] */ 'at0007',
            {
              items /* [0..1] */: [
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-OBSERVATION.symptom_sign_screening.v0]/protocol[at0007]/items[at0021] */ 'at0021',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      OBSERVATION(
        /* Travel event  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-OBSERVATION.travel_event.v0',
          data /* [1..1] */: HISTORY(
            /* History  [1..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001] */ 'at0001',
            {
              data: ITEM_TREE(
                /* Tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003] */ 'at0003',
                {
                  items /* [0..1] */: [
                    ELEMENT(
                      /* Travel?  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0004] */ 'at0004',
                      {
                        value /* [0..1] */: DV_CODED_TEXT({
                          definingCode: CODE_PHRASE({
                            codeString: 'at0006|at0005|at0027',
                            terminologyId: 'local'
                          })
                        })
                      }
                    ),
                    ELEMENT(
                      /* Domestic/international  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0026] */ 'at0026',
                      {
                        value /* [0..1] */: DV_CODED_TEXT({
                          definingCode: CODE_PHRASE({
                            codeString: 'at0028|at0029|at0030',
                            terminologyId: 'local'
                          })
                        })
                      }
                    ),
                    CLUSTER(
                      /* Specific trip  [0..*]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008] */ 'at0008',
                      {
                        items /* [1..1] */: [
                          ELEMENT(
                            /* Date of departure  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0009] */ 'at0009',
                            { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Description  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0022] */ 'at0022',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          CLUSTER(
                            /* Specific destination  [0..*]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010] */ 'at0010',
                            {
                              items /* [1..1] */: [
                                ELEMENT(
                                  /* Country  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010]/items[at0011] */ 'at0011',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                ),
                                ELEMENT(
                                  /* State/region  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010]/items[at0012] */ 'at0012',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                ),
                                ELEMENT(
                                  /* City  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010]/items[at0013] */ 'at0013',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                ),
                                ELEMENT(
                                  /* Specific place  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010]/items[at0031] */ 'at0031',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                ),
                                ELEMENT(
                                  /* Date of entry  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010]/items[at0014] */ 'at0014',
                                  {
                                    value /* [0..1] */: DV_DATE_TIME({
                                      value: '?'
                                    })
                                  }
                                ),
                                ELEMENT(
                                  /* Date of exit  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010]/items[at0015] */ 'at0015',
                                  {
                                    value /* [0..1] */: DV_DATE_TIME({
                                      value: '?'
                                    })
                                  }
                                ),
                                ELEMENT(
                                  /* Description  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010]/items[at0016] */ 'at0016',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                ),
                                CLUSTER(
                                  /* Additional destination details  [0..*]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010]/items[at0024] */ 'at0024',
                                  {
                                    archetypeId: '?' // /.*/
                                  }
                                ),
                                ELEMENT(
                                  /* Comment  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0010]/items[at0017] */ 'at0017',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                )
                              ]
                            }
                          ),
                          CLUSTER(
                            /* Additional trip details  [0..*]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0025] */ 'at0025',
                            {
                              archetypeId: '?' // /.*/
                            }
                          ),
                          ELEMENT(
                            /* Return transport  [0..*]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0018] */ 'at0018',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Date of return  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0019] */ 'at0019',
                            { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Comment  [0..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008]/items[at0020] */ 'at0020',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          )
                        ]
                      }
                    )
                  ]
                }
              )
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Item tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/protocol[at0007] */ 'at0007',
            {
              items /* [0..1] */: [
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-OBSERVATION.travel_event.v0]/protocol[at0007]/items[at0021] */ 'at0021',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      EVALUATION(
        /* Occupation summary  [0..1]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-EVALUATION.occupation_summary.v1',
          data /* [1..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001] */ 'at0001',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Description  [0..1]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[at0002] */ 'at0002',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Employment status  [0..*]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[at0004] */ 'at0004',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Occupation episode  [0..*]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[at0003] */ 'at0003',
                  {
                    archetypeId: '?' // /openEHR-EHR-CLUSTER\.occupation_record(-[a-zA-Z0-9_]+)*\.v1/
                  }
                ),
                CLUSTER(
                  /* Location-based exposure  [0..*]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.outbreak_exposure.v0] */ 'at0000',
                  {
                    archetypeId: 'openEHR-EHR-CLUSTER.outbreak_exposure.v0',
                    items /* [1..1] */: [
                      ELEMENT(
                        /* Outbreak  location  [0..*]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.outbreak_exposure.v0]/items[at0007] */ 'at0007',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString: 'at0026|at0027|at0028',
                              terminologyId: 'local'
                            })
                          })
                        }
                      ),
                      ELEMENT(
                        /* Location identifier  [0..*]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.outbreak_exposure.v0]/items[at0021] */ 'at0021',
                        { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Risk category  [0..1]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.outbreak_exposure.v0]/items[at0024] */ 'at0024',
                        { value /* [0..1] */: DV_CODED_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Date entered location  [0..1]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.outbreak_exposure.v0]/items[at0022] */ 'at0022',
                        { value /* [0..1] */: DV_DATE({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Date left location  [0..1]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.outbreak_exposure.v0]/items[at0023] */ 'at0023',
                        { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Sub-location  [0..*]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.outbreak_exposure.v0]/items[at0025] */ 'at0025',
                        {
                          archetypeId: '?' // /.*|openEHR-EHR-CLUSTER\.outbreak_exposure\.v0/
                        }
                      )
                    ]
                  }
                ),
                ELEMENT(
                  /* Comment  [0..1]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/data[at0001]/items[at0006] */ 'at0006',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                )
              ]
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/protocol[at0007] */ 'at0007',
            {
              items /* [0..1] */: [
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/protocol[at0007]/items[at0008] */ 'at0008',
                  {
                    archetypeId: '?' // /.*/
                  }
                ),
                ELEMENT(
                  /* Last updated  [0..1]  -- /content[openEHR-EHR-EVALUATION.occupation_summary.v1]/protocol[at0007]/items[at0009] */ 'at0009',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                )
              ]
            }
          )
        }
      ),
      OBSERVATION(
        /* Story/History  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-OBSERVATION.story.v1',
          data /* [1..1] */: HISTORY(
            /* Event Series  [1..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001] */ 'at0001',
            {
              data: ITEM_TREE(
                /* Tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003] */ 'at0003',
                {
                  items /* [0..1] */: [
                    ELEMENT(
                      /* Story  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0004] */ 'at0004',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    CLUSTER(
                      /* Symptom/Sign  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1] */ 'at0000',
                      {
                        archetypeId: 'openEHR-EHR-CLUSTER.symptom_sign.v1',
                        items /* [1..1] */: [
                          ELEMENT(
                            /* Symptom/Sign name  [1..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0001] */ 'at0001',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Nil significant  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0035] */ 'at0035',
                            { value /* [0..1] */: DV_BOOLEAN({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Body site  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0151] */ 'at0151',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          CLUSTER(
                            /* Structured body site  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0147] */ 'at0147',
                            {
                              archetypeId: '?' // /openEHR-EHR-CLUSTER\.anatomical_location(-[a-zA-Z0-9_]+)*\.v1|openEHR-EHR-CLUSTER.anatomical_location_circle(-[a-zA-Z0-9_]+)*\.v1|openEHR-EHR-CLUSTER\.anatomical_location_relative(-[a-zA-Z0-9_]+)*\.v1/
                            }
                          ),
                          ELEMENT(
                            /* Description  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0002] */ 'at0002',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Episodicity  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0175] */ 'at0175',
                            {
                              value /* [0..1] */: DV_CODED_TEXT({
                                definingCode: CODE_PHRASE({
                                  codeString: 'at0176|at0178|at0177',
                                  terminologyId: 'local'
                                })
                              })
                            }
                          ),
                          ELEMENT(
                            /* Occurrence  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0186] */ 'at0186',
                            {
                              value /* [0..1] */: DV_CODED_TEXT({
                                definingCode: CODE_PHRASE({
                                  codeString: 'at0187|at0188',
                                  terminologyId: 'local'
                                })
                              })
                            }
                          ),
                          ELEMENT(
                            /* Episode onset  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0152] */ 'at0152',
                            { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Onset type  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0164] */ 'at0164',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Duration  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0028] */ 'at0028',
                            { value /* [0..1] */: DV_DURATION({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Severity category  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0021] */ 'at0021',
                            {
                              value /* [0..1] */: DV_CODED_TEXT({
                                definingCode: CODE_PHRASE({
                                  codeString: 'at0023|at0024|at0025',
                                  terminologyId: 'local'
                                })
                              })
                            }
                          ),
                          ELEMENT(
                            /* Severity rating  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0026] */ 'at0026',
                            { value /* [0..1] */: DV_QUANTITY({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Character  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0189] */ 'at0189',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Progression  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0180] */ 'at0180',
                            {
                              value /* [0..1] */: DV_CODED_TEXT({
                                definingCode: CODE_PHRASE({
                                  codeString: 'at0183|at0182|at0181|at0184',
                                  terminologyId: 'local'
                                })
                              })
                            }
                          ),
                          ELEMENT(
                            /* Pattern  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0003] */ 'at0003',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          CLUSTER(
                            /* Modifying factor  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0018] */ 'at0018',
                            {
                              items /* [1..1] */: [
                                ELEMENT(
                                  /* Factor  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0018]/items[at0019] */ 'at0019',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                ),
                                ELEMENT(
                                  /* Effect  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0018]/items[at0017] */ 'at0017',
                                  {
                                    value /* [0..1] */: DV_CODED_TEXT({
                                      definingCode: CODE_PHRASE({
                                        codeString: 'at0159|at0156|at0158',
                                        terminologyId: 'local'
                                      })
                                    })
                                  }
                                ),
                                ELEMENT(
                                  /* Description  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0018]/items[at0056] */ 'at0056',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                )
                              ]
                            }
                          ),
                          CLUSTER(
                            /* Precipitating/resolving factor  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0165] */ 'at0165',
                            {
                              name /* [1..1] */: DV_CODED_TEXT({
                                definingCode: CODE_PHRASE({
                                  codeString: 'at0167|at0168',
                                  terminologyId: 'local'
                                })
                              }),
                              items /* [1..1] */: [
                                ELEMENT(
                                  /* Factor  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0165]/items[at0170] */ 'at0170',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                ),
                                CLUSTER(
                                  /* Factor detail  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0165]/items[at0154] */ 'at0154',
                                  {
                                    archetypeId: '?' // /openEHR-EHR-CLUSTER\.health_event(-[a-zA-Z0-9_]+)*\.v0|openEHR-EHR-CLUSTER\.symptom_sign(-[a-zA-Z0-9_]+)*\.v1/
                                  }
                                ),
                                ELEMENT(
                                  /* Time interval  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0165]/items[at0171] */ 'at0171',
                                  {
                                    value /* [0..1] */: DV_DURATION({
                                      value: '?'
                                    })
                                  }
                                ),
                                ELEMENT(
                                  /* Description  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0165]/items[at0185] */ 'at0185',
                                  {
                                    value /* [0..1] */: DV_TEXT({ value: '?' })
                                  }
                                )
                              ]
                            }
                          ),
                          ELEMENT(
                            /* Impact  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0155] */ 'at0155',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Episode description  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0037] */ 'at0037',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          CLUSTER(
                            /* Specific details  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0153] */ 'at0153',
                            {
                              archetypeId: '?' // /.*/
                            }
                          ),
                          ELEMENT(
                            /* Resolution date/time  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0161] */ 'at0161',
                            { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Description of previous episodes  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0057] */ 'at0057',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          ),
                          ELEMENT(
                            /* Number of previous episodes  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0031] */ 'at0031',
                            { value /* [0..1] */: DV_COUNT({ value: '?' }) }
                          ),
                          CLUSTER(
                            /* Previous episodes  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0146] */ 'at0146',
                            {
                              archetypeId: '?' // /openEHR-EHR-CLUSTER\.symptom_sign(-[a-zA-Z0-9_]+)*\.v1/
                            }
                          ),
                          CLUSTER(
                            /* Associated symptom/sign  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0063] */ 'at0063',
                            {
                              archetypeId: '?' // /openEHR-EHR-CLUSTER\.symptom_sign(-[a-zA-Z0-9_]+)*\.v1/
                            }
                          ),
                          ELEMENT(
                            /* Comment  [0..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.symptom_sign.v1]/items[at0163] */ 'at0163',
                            { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                          )
                        ]
                      }
                    )
                  ]
                }
              )
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/protocol[at0007] */ 'at0007',
            {
              items /* [0..1] */: [
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-OBSERVATION.story.v1]/protocol[at0007]/items[at0008] */ 'at0008',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      EVALUATION(
        /* Health risk assessment  [0..*]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-EVALUATION.health_risk.v1',
          data /* [1..1] */: ITEM_TREE(
            /* structure  [1..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001] */ 'at0001',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Health risk  [1..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0002] */ 'at0002',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Risk factors  [0..*]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0016] */ 'at0016',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* Risk factor  [1..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0016]/items[at0013] */ 'at0013',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Presence  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0016]/items[at0017] */ 'at0017',
                        {
                          value /* [0..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString: 'at0018|at0026|at0019',
                              terminologyId: 'local'
                            })
                          })
                        }
                      ),
                      ELEMENT(
                        /* Description  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0016]/items[at0014] */ 'at0014',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Date identified  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0016]/items[at0029] */ 'at0029',
                        { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Mitigated  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0016]/items[at0028] */ 'at0028',
                        { value /* [0..1] */: DV_BOOLEAN({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Link to evidence  [0..*]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0016]/items[at0012] */ 'at0012',
                        { value /* [0..1] */: DV_URI({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Detail  [0..*]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0016]/items[at0027] */ 'at0027',
                        {
                          archetypeId: '?' // /openEHR-EHR-CLUSTER\.family_prevalence\.v1/
                        }
                      ),
                      ELEMENT(
                        /* Comment  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0016]/items[at0030] */ 'at0030',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      )
                    ]
                  }
                ),
                ELEMENT(
                  /* Risk assessment  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0003] */ 'at0003',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Assessment type  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0020] */ 'at0020',
                  {
                    value /* [0..1] */: DV_CODED_TEXT({
                      definingCode: CODE_PHRASE({
                        codeString: 'at0021|at0022',
                        terminologyId: 'local'
                      })
                    })
                  }
                ),
                ELEMENT(
                  /* Time period  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0023] */ 'at0023',
                  { value /* [0..1] */: DV_DURATION({ value: '?' }) }
                ),
                ELEMENT(
                  /* Rationale  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0004] */ 'at0004',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Comment  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/data[at0001]/items[at0015] */ 'at0015',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                )
              ]
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/protocol[at0010] */ 'at0010',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Last updated  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/protocol[at0010]/items[at0024] */ 'at0024',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Assessment method  [0..1]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/protocol[at0010]/items[at0025] */ 'at0025',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-EVALUATION.health_risk.v1]/protocol[at0010]/items[at0011] */ 'at0011',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      EVALUATION(
        /* Problem/Diagnosis  [0..*]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-EVALUATION.problem_diagnosis.v1',
          data /* [1..1] */: ITEM_TREE(
            /* structure  [1..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001] */ 'at0001',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Problem/Diagnosis name  [1..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0002] */ 'at0002',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Clinical description  [0..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0009] */ 'at0009',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Body site  [0..*]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0012] */ 'at0012',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Structured body site  [0..*]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0039] */ 'at0039',
                  {
                    archetypeId: '?' // /openEHR-EHR-CLUSTER\.anatomical_location(-[a-zA-Z0-9_]+)*\.v1|openEHR-EHR-CLUSTER\.anatomical_location_clock(-[a-zA-Z0-9_]+)*\.v0|openEHR-EHR-CLUSTER\.anatomical_location_relative(-[a-zA-Z0-9_]+)*\.v1/
                  }
                ),
                ELEMENT(
                  /* Date/time of onset  [0..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0077] */ 'at0077',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Date/time clinically recognised  [0..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0003] */ 'at0003',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Severity  [0..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0005] */ 'at0005',
                  {
                    value /* [0..1] */: DV_CODED_TEXT({
                      definingCode: CODE_PHRASE({
                        codeString: 'at0047|at0048|at0049',
                        terminologyId: 'local'
                      })
                    })
                  }
                ),
                CLUSTER(
                  /* Specific details  [0..*]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0043] */ 'at0043',
                  {
                    archetypeId: '?' // /.*/
                  }
                ),
                ELEMENT(
                  /* Course description  [0..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0072] */ 'at0072',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Date/time of resolution  [0..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0030] */ 'at0030',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                CLUSTER(
                  /* Status  [0..*]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0046] */ 'at0046',
                  {
                    archetypeId: '?' // /openEHR-EHR-CLUSTER\.problem_qualifier(-[a-zA-Z0-9_]+)*\.v0|openEHR-EHR-CLUSTER\.problem_qualifier(-[a-zA-Z0-9_]+)*\.v1/
                  }
                ),
                ELEMENT(
                  /* Diagnostic certainty  [0..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0073] */ 'at0073',
                  {
                    value /* [0..1] */: DV_CODED_TEXT({
                      definingCode: CODE_PHRASE({
                        codeString: 'at0074|at0075|at0076',
                        terminologyId: 'local'
                      })
                    })
                  }
                ),
                ELEMENT(
                  /* Comment  [0..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/data[at0001]/items[at0069] */ 'at0069',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                )
              ]
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/protocol[at0032] */ 'at0032',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Last updated  [0..1]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/protocol[at0032]/items[at0070] */ 'at0070',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-EVALUATION.problem_diagnosis.v1]/protocol[at0032]/items[at0071] */ 'at0071',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      OBSERVATION(
        /* Imaging examination result  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-OBSERVATION.imaging_exam_result.v0',
          data /* [1..1] */: HISTORY(
            /* Event Series  [1..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001] */ 'at0001',
            {
              data: ITEM_TREE(
                /* Tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003] */ 'at0003',
                {
                  items /* [0..1] */: [
                    ELEMENT(
                      /* Test name  [1..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0004] */ 'at0004',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Modality  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0005] */ 'at0005',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Anatomical site  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0055] */ 'at0055',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    CLUSTER(
                      /* Structured anatomical site  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0006] */ 'at0006',
                      {
                        archetypeId: '?' // /openEHR-EHR-CLUSTER\.anatomical_location(-[a-zA-Z0-9_]+)*\.v1/
                      }
                    ),
                    ELEMENT(
                      /* Overall result status  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0007] */ 'at0007',
                      {
                        value /* [0..1] */: DV_CODED_TEXT({
                          definingCode: CODE_PHRASE({
                            codeString: 'at0009|at0010|at0011|at0012|at0013',
                            terminologyId: 'local'
                          })
                        })
                      }
                    ),
                    ELEMENT(
                      /* DateTime result issued  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0024] */ 'at0024',
                      { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Clinical information provided  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0014] */ 'at0014',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Findings  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0008] */ 'at0008',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    CLUSTER(
                      /* Imaging findings  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0044] */ 'at0044',
                      {
                        archetypeId: '?' // /openEHR-EHR-CLUSTER\.imaging_finding\.v0/
                      }
                    ),
                    ELEMENT(
                      /* Comparison with previous  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0056] */ 'at0056',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Conclusion  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0021] */ 'at0021',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Imaging differential diagnosis  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0058] */ 'at0058',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Imaging diagnosis  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0020] */ 'at0020',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    CLUSTER(
                      /* Image representation  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0045] */ 'at0045',
                      {
                        archetypeId: '?' // /openEHR-EHR-CLUSTER\.multimedia(-[a-zA-Z0-9_]+)*\.v1/
                      }
                    ),
                    ELEMENT(
                      /* Recommendation  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0059] */ 'at0059',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Comment  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/data[at0001]/events[at0002]/data[at0003]/items[at0023] */ 'at0023',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    )
                  ]
                }
              )
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025] */ 'at0025',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Technique  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0049] */ 'at0049',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Structured technique  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0041] */ 'at0041',
                  {
                    archetypeId: '?' // /.*/
                  }
                ),
                ELEMENT(
                  /* Imaging quality  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0057] */ 'at0057',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Receiving imaging service  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0026] */ 'at0026',
                  {
                    archetypeId: '?' // /.*/
                  }
                ),
                CLUSTER(
                  /* Examination request details  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027] */ 'at0027',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* Requester order identifier  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0028] */ 'at0028',
                        { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Examination requested name  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0029] */ 'at0029',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Requester  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0030] */ 'at0030',
                        {
                          archetypeId: '?' // /.*/
                        }
                      ),
                      ELEMENT(
                        /* Receiver order identifier  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0031] */ 'at0031',
                        { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* DICOM study identifier  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0032] */ 'at0032',
                        { value /* [0..1] */: DV_URI({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Report identifier  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0033] */ 'at0033',
                        { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Image details  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0034] */ 'at0034',
                        {
                          items /* [1..1] */: [
                            ELEMENT(
                              /* Image identifier  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0034]/items[at0035] */ 'at0035',
                              {
                                value /* [0..1] */: DV_IDENTIFIER({
                                  value: '?'
                                })
                              }
                            ),
                            ELEMENT(
                              /* DICOM series identifier  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0034]/items[at0036] */ 'at0036',
                              {
                                value /* [0..1] */: DV_IDENTIFIER({
                                  value: '?'
                                })
                              }
                            ),
                            ELEMENT(
                              /* View  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0034]/items[at0037] */ 'at0037',
                              { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                            ),
                            ELEMENT(
                              /* Position  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0034]/items[at0038] */ 'at0038',
                              { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                            ),
                            ELEMENT(
                              /* Image DateTime  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0034]/items[at0039] */ 'at0039',
                              {
                                value /* [0..1] */: DV_DATE_TIME({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Image  [0..1]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0027]/items[at0034]/items[at0040] */ 'at0040',
                              {
                                value /* [0..1] */: DV_MULTIMEDIA({
                                  value: '?'
                                })
                              }
                            )
                          ],
                          name /* [1..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString: 'at0053|at0054',
                              terminologyId: 'local'
                            })
                          })
                        }
                      )
                    ]
                  }
                ),
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]/protocol[at0025]/items[at0046] */ 'at0046',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      OBSERVATION(
        /* Laboratory test result  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-OBSERVATION.laboratory_test_result.v1',
          data /* [1..1] */: HISTORY(
            /* Event Series  [1..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001] */ 'at0001',
            {
              data: ITEM_TREE(
                /* Tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003] */ 'at0003',
                {
                  items /* [0..1] */: [
                    ELEMENT(
                      /* Test name  [1..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0005] */ 'at0005',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    CLUSTER(
                      /* Specimen detail  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0065] */ 'at0065',
                      {
                        archetypeId: '?' // /openEHR-EHR-CLUSTER\.specimen(-[a-zA-Z0-9_]+)*\.v0|openEHR-EHR-CLUSTER\.specimen(-[a-zA-Z0-9_]+)*\.v1/
                      }
                    ),
                    ELEMENT(
                      /* Overall test status  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0073] */ 'at0073',
                      {
                        value /* [0..1] */: DV_CODED_TEXT({
                          definingCode: CODE_PHRASE({
                            codeString:
                              'at0107|at0037|at0120|at0038|at0040|at0115|at0119|at0074|at0116',
                            terminologyId: 'local'
                          })
                        })
                      }
                    ),
                    ELEMENT(
                      /* Overall test status timestamp  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0075] */ 'at0075',
                      { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Diagnostic service category  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0077] */ 'at0077',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Clinical information provided  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0100] */ 'at0100',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    CLUSTER(
                      /* Test result  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0097] */ 'at0097',
                      {
                        archetypeId: '?' // /openEHR-EHR-CLUSTER\.laboratory_test_analyte(-[a-zA-Z0-9_]+)*\.v0|openEHR-EHR-CLUSTER\.laboratory_test_analyte(-[a-zA-Z0-9_]+)*\.v1|openEHR-EHR-CLUSTER\.laboratory_test_panel(-[a-zA-Z0-9_]+)*\.v0|openEHR-EHR-CLUSTER\.laboratory_test_panel(-[a-zA-Z0-9_]+)*\.v1|openEHR-EHR-CLUSTER\.histopathology_findings(-[a-zA-Z0-9_]+)*\.v0|openEHR-EHR-CLUSTER\.histopathology_findings(-[a-zA-Z0-9_]+)*\.v1/
                      }
                    ),
                    ELEMENT(
                      /* Conclusion  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0057] */ 'at0057',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    ELEMENT(
                      /* Test diagnosis  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0098] */ 'at0098',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    ),
                    CLUSTER(
                      /* Structured test diagnosis  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0122] */ 'at0122',
                      {
                        archetypeId: '?' // /.*/
                      }
                    ),
                    CLUSTER(
                      /* Multimedia representation  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0118] */ 'at0118',
                      {
                        archetypeId: '?' // /openEHR-EHR-CLUSTER\.multimedia(-[a-zA-Z0-9_]+)*\.v1/
                      }
                    ),
                    ELEMENT(
                      /* Comment  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0101] */ 'at0101',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    )
                  ]
                }
              )
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004] */ 'at0004',
            {
              items /* [0..1] */: [
                CLUSTER(
                  /* Receiving laboratory  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0017] */ 'at0017',
                  {
                    archetypeId: '?' // /.*/
                  }
                ),
                ELEMENT(
                  /* Laboratory internal identifier  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0068] */ 'at0068',
                  { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                ),
                CLUSTER(
                  /* Test request details  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0094] */ 'at0094',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* Original test requested name  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0094]/items[at0106] */ 'at0106',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Requester order identifier  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0094]/items[at0062] */ 'at0062',
                        { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Receiver order identifier  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0094]/items[at0063] */ 'at0063',
                        { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Requester  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0094]/items[at0090] */ 'at0090',
                        {
                          archetypeId: '?' // /.*/
                        }
                      ),
                      CLUSTER(
                        /* Distribution list  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0094]/items[at0035] */ 'at0035',
                        {
                          archetypeId: '?' // /openEHR-EHR-CLUSTER\.distribution(-[a-zA-Z0-9_]+)*\.v1/
                        }
                      )
                    ]
                  }
                ),
                ELEMENT(
                  /* Point-of-care test  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0111] */ 'at0111',
                  { value /* [0..1] */: DV_BOOLEAN({ value: '?' }) }
                ),
                ELEMENT(
                  /* Test method  [0..1]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0121] */ 'at0121',
                  {}
                ),
                CLUSTER(
                  /* Testing details  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0110] */ 'at0110',
                  {
                    archetypeId: '?' // /openEHR-EHR-CLUSTER\.device(-[a-zA-Z0-9_]+)*\.v1/
                  }
                ),
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-OBSERVATION.laboratory_test_result.v1]/protocol[at0004]/items[at0117] */ 'at0117',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      ACTION(
        /* Medication management  [0..*]  -- /content[openEHR-EHR-ACTION.medication.v1] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-ACTION.medication.v1',
          ism_transition /* [1..1] */: ISM_TRANSITION(
            /* Medication recommended  [1..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/ism_transition[at0109] */ 'at0109',
            {
              current_state /* [1..1] */: DV_CODED_TEXT({
                definingCode: CODE_PHRASE({
                  codeString: '526',
                  terminologyId: 'openehr'
                })
              }),
              careflow_step /* [0..1] */: DV_CODED_TEXT({
                definingCode: CODE_PHRASE({
                  codeString: 'at0109',
                  terminologyId: 'local'
                })
              })
            }
          ),
          description /* [1..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017] */ 'at0017',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Medication item  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0020] */ 'at0020',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Medication details  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0104] */ 'at0104',
                  {
                    archetypeId: '?' // /openEHR-EHR-CLUSTER\.medication(-[a-zA-Z0-9_]+)*\.v1/
                  }
                ),
                CLUSTER(
                  /* Amount  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0131] */ 'at0131',
                  {
                    archetypeId: '?' // /openEHR-EHR-CLUSTER\.medication_supply_amount(-[a-zA-Z0-9_]+)*\.v0|openEHR-EHR-CLUSTER\.medication_supply_amount(-[a-zA-Z0-9_]+)*\.v1|openEHR-EHR-CLUSTER\.dosage(-[a-zA-Z0-9_]+)*\.v1/
                  }
                ),
                ELEMENT(
                  /* Substitution  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0132] */ 'at0132',
                  {
                    value /* [0..1] */: DV_CODED_TEXT({
                      definingCode: CODE_PHRASE({
                        codeString: 'at0138|at0139',
                        terminologyId: 'local'
                      })
                    })
                  }
                ),
                ELEMENT(
                  /* Substitution reason  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0133] */ 'at0133',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Original scheduled date/time  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0043] */ 'at0043',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Restart date/time  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0154] */ 'at0154',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Restart criterion  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0155] */ 'at0155',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Reason  [0..*]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0021] */ 'at0021',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Administration details  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0140] */ 'at0140',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* Route  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0140]/items[at0147] */ 'at0147',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Body site  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0140]/items[at0141] */ 'at0141',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Structured body site  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0140]/items[at0142] */ 'at0142',
                        {
                          archetypeId: '?' // /openEHR-EHR-CLUSTER\.anatomical_location(-[a-zA-Z0-9_]+)*\.v1|openEHR-EHR-CLUSTER\.anatomical_location_circle(-[a-zA-Z0-9_]+)*\.v1|openEHR-EHR-CLUSTER\.anatomical_location_relative(-[a-zA-Z0-9_]+)*\.v1/
                        }
                      ),
                      ELEMENT(
                        /* Administration method  [0..*]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0140]/items[at0143] */ 'at0143',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Administration device  [0..*]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0140]/items[at0144] */ 'at0144',
                        {
                          archetypeId: '?' // /openEHR-EHR-CLUSTER\.device(-[a-zA-Z0-9_]+)*\.v1/
                        }
                      )
                    ]
                  }
                ),
                CLUSTER(
                  /* Additional details  [0..*]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0053] */ 'at0053',
                  {
                    archetypeId: '?' // /openEHR-EHR-CLUSTER\.medication_authorisation(-[a-zA-Z0-9_]+)*\.v0|openEHR-EHR-CLUSTER\.medication_authorisation(-[a-zA-Z0-9_]+)*\.v1/
                  }
                ),
                ELEMENT(
                  /* Patient guidance  [0..*]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0033] */ 'at0033',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Double-checked?  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0149] */ 'at0149',
                  { value /* [0..1] */: DV_BOOLEAN({ value: '?' }) }
                ),
                ELEMENT(
                  /* Sequence number  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0025] */ 'at0025',
                  { value /* [0..1] */: DV_COUNT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Comment  [0..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/description[at0017]/items[at0024] */ 'at0024',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                )
              ]
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-ACTION.medication.v1]/protocol[at0030] */ 'at0030',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Order ID  [0..*]  -- /content[openEHR-EHR-ACTION.medication.v1]/protocol[at0030]/items[at0103] */ 'at0103',
                  { value /* [0..1] */: DV_IDENTIFIER({ value: '?' }) }
                ),
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-ACTION.medication.v1]/protocol[at0030]/items[at0085] */ 'at0085',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      ADMIN_ENTRY(
        /* Patient admission  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-ADMIN_ENTRY.admission.v0',
          data /* [1..1] */: ITEM_TREE(
            /* Tree  [1..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001] */ 'at0001',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Patient class  [1..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0002] */ 'at0002',
                  {
                    value /* [0..1] */: DV_CODED_TEXT({
                      definingCode: CODE_PHRASE({
                        codeString:
                          'at0003|at0004|at0005|at0006|at0007|at0008|at0009|at0010|at0011',
                        terminologyId: 'local'
                      })
                    })
                  }
                ),
                CLUSTER(
                  /* Assigned patient location  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073] */ 'at0073',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* Point of care/Unit  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0074] */ 'at0074',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Ward  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0077] */ 'at0077',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Room  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0078] */ 'at0078',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Bed  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0079] */ 'at0079',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Facility  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0104] */ 'at0104',
                        {
                          items /* [1..1] */: [
                            ELEMENT(
                              /* Namespace ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0104]/items[at0105] */ 'at0105',
                              { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                            ),
                            ELEMENT(
                              /* Universal ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0104]/items[at0106] */ 'at0106',
                              { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                            ),
                            ELEMENT(
                              /* Universal ID type  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0104]/items[at0107] */ 'at0107',
                              {
                                value /* [0..1] */: DV_CODED_TEXT({
                                  definingCode: CODE_PHRASE({
                                    codeString:
                                      'at0108|at0109|at0110|at0111|at0112|at0113|at0114|at0115|at0116|at0118',
                                    terminologyId: 'local'
                                  })
                                })
                              }
                            )
                          ]
                        }
                      ),
                      ELEMENT(
                        /* Building  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0101] */ 'at0101',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Floor  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0102] */ 'at0102',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Location description  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0103] */ 'at0103',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Address  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0084] */ 'at0084',
                        {
                          items /* [1..1] */: [
                            ELEMENT(
                              /* Street  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0084]/items[at0085] */ 'at0085',
                              { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                            ),
                            ELEMENT(
                              /* City  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0084]/items[at0086] */ 'at0086',
                              { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                            ),
                            ELEMENT(
                              /* State/province  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0084]/items[at0087] */ 'at0087',
                              { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                            ),
                            ELEMENT(
                              /* Post code  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0084]/items[at0088] */ 'at0088',
                              { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                            ),
                            ELEMENT(
                              /* Country  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0084]/items[at0089] */ 'at0089',
                              { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                            )
                          ]
                        }
                      ),
                      ELEMENT(
                        /* Location type  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0073]/items[at0081] */ 'at0081',
                        {
                          value /* [0..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString: 'at0082|at0090|at0091|at0092|at0093',
                              terminologyId: 'local'
                            })
                          })
                        }
                      )
                    ]
                  }
                ),
                ELEMENT(
                  /* Admission type  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0013] */ 'at0013',
                  {
                    value /* [0..1] */: DV_CODED_TEXT({
                      definingCode: CODE_PHRASE({
                        codeString:
                          'at0014|at0015|at0016|at0017|at0018|at0019|at0020|at0021|at0022',
                        terminologyId: 'local'
                      })
                    })
                  }
                ),
                ELEMENT(
                  /* Pre-admit number  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0023] */ 'at0023',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Prior patient location  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094] */ 'at0094',
                  {
                    items /* [1..1] */: [
                      CLUSTER(
                        /* Facility  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0119] */ 'at0119',
                        {
                          items /* [1..1] */: [
                            ELEMENT(
                              /* Namespace ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0119]/items[at0105] */ 'at0105',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Namespace ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0119]/items[at0105] */ 'at0105',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Universal ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0119]/items[at0106] */ 'at0106',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Universal ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0119]/items[at0106] */ 'at0106',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Universal ID type  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0119]/items[at0107] */ 'at0107',
                              {
                                value /* [0..1] */: DV_CODED_TEXT({
                                  definingCode: CODE_PHRASE({
                                    codeString:
                                      'at0108|at0109|at0110|at0111|at0112|at0113|at0114|at0115|at0116|at0118',
                                    terminologyId: 'local'
                                  })
                                }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Universal ID type  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0119]/items[at0107] */ 'at0107',
                              {
                                value /* [0..1] */: DV_CODED_TEXT({
                                  definingCode: CODE_PHRASE({
                                    codeString:
                                      'at0108|at0109|at0110|at0111|at0112|at0113|at0114|at0115|at0116|at0118',
                                    terminologyId: 'local'
                                  })
                                }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            )
                          ]
                        }
                      ),
                      CLUSTER(
                        /* Adress  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095] */ 'at0095',
                        {
                          items /* [1..1] */: [
                            ELEMENT(
                              /* Street  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0085] */ 'at0085',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Street  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0085] */ 'at0085',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* City  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0086] */ 'at0086',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* City  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0086] */ 'at0086',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* State/province  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0087] */ 'at0087',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* State/province  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0087] */ 'at0087',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Post code  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0088] */ 'at0088',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Post code  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0088] */ 'at0088',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Country  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0089] */ 'at0089',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            ),
                            ELEMENT(
                              /* Country  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0095]/items[at0089] */ 'at0089',
                              {
                                value /* [0..1] */: DV_TEXT({ value: '?' }),
                                name /* [1..1] */: DV_TEXT({ value: '?' })
                              }
                            )
                          ]
                        }
                      ),
                      ELEMENT(
                        /* Point of care/Unit  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0074] */ 'at0074',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Point of care/Unit  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0074] */ 'at0074',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Ward  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0077] */ 'at0077',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Ward  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0077] */ 'at0077',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Room  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0078] */ 'at0078',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Room  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0078] */ 'at0078',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Bed  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0079] */ 'at0079',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Bed  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0079] */ 'at0079',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Building  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0101] */ 'at0101',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Building  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0101] */ 'at0101',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Floor  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0102] */ 'at0102',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Floor  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0102] */ 'at0102',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Location description  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0103] */ 'at0103',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Location description  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0094]/items[at0103] */ 'at0103',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      )
                    ]
                  }
                ),
                CLUSTER(
                  /* Attending doctor  [0..*]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0098] */ 'at0098',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0098]/items[at0099] */ 'at0099',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Family name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0098]/items[at0100] */ 'at0100',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Last name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0098]/items[at0120] */ 'at0120',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      )
                    ]
                  }
                ),
                CLUSTER(
                  /* Referring doctor  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0025] */ 'at0025',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0025]/items[at0099] */ 'at0099',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0025]/items[at0099] */ 'at0099',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Family name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0025]/items[at0100] */ 'at0100',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Family name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0025]/items[at0100] */ 'at0100',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Last name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0025]/items[at0120] */ 'at0120',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Last name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0025]/items[at0120] */ 'at0120',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      )
                    ]
                  }
                ),
                CLUSTER(
                  /* Consulting doctor  [0..*]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0121] */ 'at0121',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0121]/items[at0099] */ 'at0099',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0121]/items[at0099] */ 'at0099',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Family name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0121]/items[at0100] */ 'at0100',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Family name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0121]/items[at0100] */ 'at0100',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Last name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0121]/items[at0120] */ 'at0120',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Last name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0121]/items[at0120] */ 'at0120',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      )
                    ]
                  }
                ),
                ELEMENT(
                  /* Hospital service  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0041] */ 'at0041',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Admit source  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0049] */ 'at0049',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Admitting doctor  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0051] */ 'at0051',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0051]/items[at0099] */ 'at0099',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* ID  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0051]/items[at0099] */ 'at0099',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Family name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0051]/items[at0100] */ 'at0100',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Family name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0051]/items[at0100] */ 'at0100',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Last name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0051]/items[at0120] */ 'at0120',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Last name  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0051]/items[at0120] */ 'at0120',
                        {
                          value /* [0..1] */: DV_TEXT({ value: '?' }),
                          name /* [1..1] */: DV_TEXT({ value: '?' })
                        }
                      )
                    ]
                  }
                ),
                ELEMENT(
                  /* Financial class  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0061] */ 'at0061',
                  {
                    value /* [0..1] */: DV_CODED_TEXT({
                      definingCode: CODE_PHRASE({
                        codeString:
                          'at0062|at0063|at0064|at0065|at0122|at0123|at0124|at0125|at0126|at0127|at0128|at0129|at0130',
                        terminologyId: 'local'
                      })
                    })
                  }
                ),
                ELEMENT(
                  /* Charge price indicator  [0..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0066] */ 'at0066',
                  { value /* [0..1] */: DV_CODED_TEXT({}) }
                ),
                ELEMENT(
                  /* Admit date/time  [1..1]  -- /content[openEHR-EHR-ADMIN_ENTRY.admission.v0]/data[at0001]/items[at0071] */ 'at0071',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                )
              ]
            }
          )
        }
      ),
      EVALUATION(
        /* Advance care directive  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-EVALUATION.advance_care_directive.v1',
          data /* [1..1] */: ITEM_TREE(
            /* Item tree  [1..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001] */ 'at0001',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Type of directive  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[at0005] */ 'at0005',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Status  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[at0004] */ 'at0004',
                  {
                    value /* [0..1] */: DV_CODED_TEXT({
                      definingCode: CODE_PHRASE({
                        codeString: 'at0044|at0045|at0047',
                        terminologyId: 'local'
                      })
                    })
                  }
                ),
                ELEMENT(
                  /* Description  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[at0006] */ 'at0006',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                ELEMENT(
                  /* Condition  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[at0007] */ 'at0007',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Ventilator settings/findings  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0] */ 'at0000',
                  {
                    archetypeId: 'openEHR-EHR-CLUSTER.ventilator_settings2.v0',
                    items /* [1..1] */: [
                      ELEMENT(
                        /* Type of ventilation  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0144] */ 'at0144',
                        {
                          value /* [0..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString: 'at0145|at0146',
                              terminologyId: 'local'
                            })
                          })
                        }
                      ),
                      ELEMENT(
                        /* Ventilation mode  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0001] */ 'at0001',
                        {
                          value /* [0..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString:
                                'at0104|at0108|at0105|at0065|at0106|at0077|at0152|at0076|at0070|at0072|at0079|at0153|at0073|at0075|at0080|at0083|at0084|at0085|at0097|at0155',
                              terminologyId: 'local'
                            })
                          })
                        }
                      ),
                      ELEMENT(
                        /* Ventilation submode  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0114] */ 'at0114',
                        {
                          value /* [0..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString:
                                'at0151|at0147|at0124|at0143|at0117|at0118|at0119|at0120|at0129|at0131|at0116|at0122|at0115|at0135|at0138|at0139|at0140|at0141|at0142',
                              terminologyId: 'local'
                            })
                          })
                        }
                      ),
                      CLUSTER(
                        /* Ventilation device  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0056] */ 'at0056',
                        {
                          archetypeId: '?' // /openEHR-EHR-CLUSTER\.device(-[a-zA-Z0-9_]+)*\.v1/
                        }
                      ),
                      CLUSTER(
                        /* Oxygen delivery  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0051] */ 'at0051',
                        {
                          archetypeId: '?' // /openEHR-EHR-CLUSTER\.gas_delivery-oxygen(-[a-zA-Z0-9_]+)*\.v1/
                        }
                      ),
                      ELEMENT(
                        /* Heater used  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0055] */ 'at0055',
                        { value /* [0..1] */: DV_BOOLEAN({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Frequency  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0007] */ 'at0007',
                        {
                          name /* [1..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString:
                                'at0010|at0111|at0008|at0009|at0156|at0161',
                              terminologyId: 'local'
                            })
                          }),
                          value /* [0..1] */: DV_QUANTITY({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Pressure  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0015] */ 'at0015',
                        {
                          name /* [1..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString:
                                'at0091|at0026|at0092|at0017|at0154|at0024|at0150|at0028|at0020|at0021|at0025|at0016|at0018|at0019|at0027|at0089|at0090|at0096|at0158|at0159|at0160|at0163|at0162',
                              terminologyId: 'local'
                            })
                          }),
                          value /* [0..1] */: DV_QUANTITY({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Volume  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0031] */ 'at0031',
                        {
                          name /* [1..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString: 'at0032|at0112|at0033|at0098',
                              terminologyId: 'local'
                            })
                          }),
                          value /* [0..1] */: DV_QUANTITY({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Flow rate  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0060] */ 'at0060',
                        {
                          name /* [1..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString: 'at0149|at0099|at0100',
                              terminologyId: 'local'
                            })
                          }),
                          value /* [0..1] */: DV_QUANTITY({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Timing  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0038] */ 'at0038',
                        {
                          name /* [1..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString: 'at0039|at0113|at0040|at0041',
                              terminologyId: 'local'
                            })
                          }),
                          value /* [0..1] */: DV_DURATION({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Trigger value  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0045] */ 'at0045',
                        {
                          name /* [1..1] */: DV_CODED_TEXT({
                            definingCode: CODE_PHRASE({
                              codeString: 'at0047|at0048|at0049|at0050|at0046',
                              terminologyId: 'local'
                            })
                          }),
                          value /* [0..1] */: DV_COUNT({ value: '?' })
                        }
                      ),
                      ELEMENT(
                        /* Trigger type  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0087] */ 'at0087',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* I:E Inspiration/expiration  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0043] */ 'at0043',
                        { value /* [0..1] */: DV_PROPORTION({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* I:T Inspiratory/Total  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0044] */ 'at0044',
                        { value /* [0..1] */: DV_PROPORTION({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* Artificial airway compensation  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0095] */ 'at0095',
                        { value /* [0..1] */: DV_PROPORTION({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* NO delivered  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0053] */ 'at0053',
                        { value /* [0..1] */: DV_QUANTITY({ value: '?' }) }
                      ),
                      ELEMENT(
                        /* NO2 removed  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[openEHR-EHR-CLUSTER.ventilator_settings2.v0]/items[at0054] */ 'at0054',
                        { value /* [0..1] */: DV_QUANTITY({ value: '?' }) }
                      )
                    ]
                  }
                ),
                CLUSTER(
                  /* Directive location  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[at0058] */ 'at0058',
                  {
                    items /* [1..1] */: [
                      ELEMENT(
                        /* Location  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[at0058]/items[at0030] */ 'at0030',
                        { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                      ),
                      CLUSTER(
                        /* Copy holder  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[at0058]/items[at0059] */ 'at0059',
                        {
                          archetypeId: '?' // /.*/
                        }
                      ),
                      CLUSTER(
                        /* Digital representation  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[at0058]/items[at0060] */ 'at0060',
                        {
                          archetypeId: '?' // /openEHR-EHR-CLUSTER\.multimedia\.v0/
                        }
                      )
                    ]
                  }
                ),
                ELEMENT(
                  /* Comment  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/data[at0001]/items[at0038] */ 'at0038',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                )
              ]
            }
          ),
          protocol /* [0..1] */: ITEM_TREE(
            /* Item tree  [1..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/protocol[at0010] */ 'at0010',
            {
              items /* [0..1] */: [
                ELEMENT(
                  /* Valid period start  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/protocol[at0010]/items[at0053] */ 'at0053',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Valid period end  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/protocol[at0010]/items[at0054] */ 'at0054',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Review due date  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/protocol[at0010]/items[at0056] */ 'at0056',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                ELEMENT(
                  /* Last updated  [0..1]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/protocol[at0010]/items[at0055] */ 'at0055',
                  { value /* [0..1] */: DV_DATE_TIME({ value: '?' }) }
                ),
                CLUSTER(
                  /* Witness  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/protocol[at0010]/items[at0025] */ 'at0025',
                  {
                    archetypeId: '?' // /.*/
                  }
                ),
                ELEMENT(
                  /* Mandate  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/protocol[at0010]/items[at0027] */ 'at0027',
                  { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                ),
                CLUSTER(
                  /* Extension  [0..*]  -- /content[openEHR-EHR-EVALUATION.advance_care_directive.v1]/protocol[at0010]/items[at0061] */ 'at0061',
                  {
                    archetypeId: '?' // /.*/
                  }
                )
              ]
            }
          )
        }
      ),
      SECTION(
        /* Conclusion  [0..*]  -- /content[openEHR-EHR-SECTION.conclusion.v0] */ 'at0000',
        {
          archetypeId: 'openEHR-EHR-SECTION.conclusion.v0',
          items /* [0..1] */: EVALUATION(
            /* Clinical synopsis  [0..*]  -- /content[openEHR-EHR-SECTION.conclusion.v0]/items[openEHR-EHR-EVALUATION.clinical_synopsis.v1] */ 'at0000',
            {
              archetypeId: 'openEHR-EHR-EVALUATION.clinical_synopsis.v1',
              data /* [1..1] */: ITEM_TREE(
                /* List  [1..1]  -- /content[openEHR-EHR-SECTION.conclusion.v0]/items[openEHR-EHR-EVALUATION.clinical_synopsis.v1]/data[at0001] */ 'at0001',
                {
                  items /* [0..1] */: [
                    ELEMENT(
                      /* Synopsis  [1..1]  -- /content[openEHR-EHR-SECTION.conclusion.v0]/items[openEHR-EHR-EVALUATION.clinical_synopsis.v1]/data[at0001]/items[at0002] */ 'at0002',
                      { value /* [0..1] */: DV_TEXT({ value: '?' }) }
                    )
                  ]
                }
              ),
              protocol /* [0..1] */: ITEM_TREE(
                /* Tree  [1..1]  -- /content[openEHR-EHR-SECTION.conclusion.v0]/items[openEHR-EHR-EVALUATION.clinical_synopsis.v1]/protocol[at0003] */ 'at0003',
                {
                  items /* [0..1] */: [
                    CLUSTER(
                      /* Extension  [0..*]  -- /content[openEHR-EHR-SECTION.conclusion.v0]/items[openEHR-EHR-EVALUATION.clinical_synopsis.v1]/protocol[at0003]/items[at0004] */ 'at0004',
                      {
                        archetypeId: '?' // /.*/
                      }
                    )
                  ]
                }
              )
            }
          )
        }
      )
    ]
  });
}
module.exports = generate;
