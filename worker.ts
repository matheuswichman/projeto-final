import amqp from 'amqplib';
import * as path from 'path';
import * as fse from 'fs-extra';
import * as fs from 'fs';
import parse from 'csv-parse';
import {
  DATASETS_DIR,
  PROJECTS_DIR,
  TEMPLATES_DIR,
  JOBS_DIR
} from './api/constants';
import applyTemplate from './core/template-applier';

const queue = 'jobs';

async function processMessage(msg) {
  const conversions = [];
  const csvPath = path.resolve(DATASETS_DIR, `${msg.datasetId}.csv`);
  const outputDir = path.resolve(JOBS_DIR, msg.id);

  const csvManifestPath = path.resolve(DATASETS_DIR, `${msg.datasetId}.json`);
  const csvManifest = fse.readFileSync(csvManifestPath, { encoding: 'utf-8' });
  const csvManifestJson = JSON.parse(csvManifest);

  const jobPath = path.resolve(JOBS_DIR, `${msg.id}.json`);
  const job = fse.readFileSync(jobPath, { encoding: 'utf-8' });
  const jobJson = JSON.parse(job);

  fse.mkdirSync(outputDir, {
    mode: 0o777,
    recursive: true
  });

  const manifestPath = path.resolve(PROJECTS_DIR, `${msg.projectId}.json`);
  const projectPath = path.resolve(PROJECTS_DIR, `${msg.projectId}.js`);
  const project = fse.readFileSync(manifestPath, { encoding: 'utf-8' });
  const projectJson = JSON.parse(project);
  const optPath = path.resolve(TEMPLATES_DIR, `${projectJson.templateId}.opt`);

  let writes = 0;
  let rowNum = 0;

  fs.createReadStream(csvPath)
    .pipe(parse({ columns: true, delimiter: ';' }))
    .on('readable', function () {
      let row;
      while ((row = this.read())) {
        const promise = new Promise<void>(async (resolve, reject) => {
          try {
            const output = await applyTemplate(
              optPath,
              projectPath,
              row,
              'json'
            );
            const fileName = `${rowNum}.json`;
            const outputPath = path.join(outputDir, fileName);
            fse.writeFileSync(outputPath, output);
            writes += 1;
            rowNum += 1;
            if (writes % 10 === 0) {
              fse.writeJSONSync(jobPath, {
                ...jobJson,
                convertedRows: writes,
                totalRows: csvManifestJson.rowCount
              });
            }
            resolve();
          } catch (error) {
            reject(error);
          }
        });
        conversions.push(promise);
      }
    })
    .on('end', async () => {
      const results = await Promise.allSettled(conversions);

      const fails = Object.values(results).filter(
        (promise): promise is PromiseRejectedResult =>
          promise.status === 'rejected'
      );
      fails.forEach((build) => {
        console.error(build.reason);
      });
      fse.writeJSONSync(jobPath, {
        ...jobJson,
        convertedRows: writes,
        totalRows: csvManifestJson.rowCount
      });
    });
}

async function main() {
  const connection = await amqp.connect('amqp://localhost');

  const channel = await connection.createChannel();
  await channel.assertQueue(queue, { durable: true });

  channel.consume(queue, async (msg) => {
    if (msg === null) {
      return;
    }
    const json = JSON.parse(msg.content.toString());
    console.log('Received:', json);
    try {
      await processMessage(json);
      channel.ack(msg);
    } catch (error) {
      console.log('Error while processing', error);
    }
  });
}

main();
